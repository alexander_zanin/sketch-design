# 1968 Sketch design project

## Build Setup

``` bash
# install dependencies (for every single directory in the "multilang" directory)
npm install

# start webpack dev server
npm start

# build js (it must be run parallel with 'npm start' command above)
# or if you want only to build js without starting the server then run this single command
npm run build

```



## Bash scripts (utils for preparing files for the hosting)

``` bash
# copy and exclude redundant files and paste it to another directory (for uploading files to hosting)
# (currently target directory is hardcoded inside bash script, edit it if you need or create new)
./copy-and-exclude.sh


# execute 'npm run build' command for every single directory in en/fr/rus directories
cd multilang/npm-run-build.sh

```
