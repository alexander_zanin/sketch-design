#!/bin/bash

for d in */ ; do
    rsync -aP --exclude=node_modules --exclude=js --exclude=package-lock.json --exclude=package.json --exclude=webpack.config.js --exclude=commonComponents --exclude=npm-run-build.sh $d/* /Users/alexanderzanin/Desktop/dist/$d
    ( cd $d && npm run build )
done