import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { showKeyboard } from '@common/mixins/show-keyboard';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import AndroidKeyboard from '@common/android-keyboard/vue-android-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';


new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'android-keyboard': AndroidKeyboard,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, showKeyboard, getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'android-telegram',
		interlocutor: {
			name: null,
			isTyping: false,
			isOnline: false
		},
		createGroupChat: false,
		participantsAmount: null,

		screenSettings: {
			currentLang: 'rus',
			avatar: './img/1a iosavatar.png',
			name: 'Michael Scott',
			lastSeen: 'last seen 5 minutes ago'
		}
	},
	methods: {
		checkTypingStatus(interlocutorIsTyping, interlocutorName) {
			this.interlocutor.isTyping = interlocutorIsTyping;
			this.interlocutor.name = interlocutorName;
		},
		checkOnlineStatus(interlocutorIsOnline) {
			this.interlocutor.isOnline = interlocutorIsOnline;
		}
	}
});