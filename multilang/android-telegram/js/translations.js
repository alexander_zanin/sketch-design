export const tr = {
	rus: {
		back: 'Чаты',
		typing: 'печатает',
		status: 'Онлайн',
		placeholder: 'Сообщение'
	},

	en: {
		back: 'Chats',
		typing: 'typing',
		status: 'Online',
		placeholder: 'Message'
	},

	fr: {
		back: 'Chats',
		typing: 'dactylographié',
		status: 'En ligne',
		placeholder: 'Message'
	}
}