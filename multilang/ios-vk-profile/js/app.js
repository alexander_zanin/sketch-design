import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';

new Vue({
	el: '#app',
	components: {
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay
	},
	mixins: [getscreenSettings],
	data: {
		databaseRef: 'ios-vk-profile',
		screenSettings: {
			name: 'Павел Князев',
			avatar: './img/avatar.png',
			state: 'online (моб.)',
			photosCount: '42 фотографии',
			meta: [
				{
					name: 'друзей',
					count: '374'
				},
				{
					name: 'общих',
					count: '69'
				},
				{
					name: 'подписчиков',
					count: '1,2K'
				},
				{
					name: 'фото',
					count: '44'
				},
				{
					name: 'видео',
					count: '12'
				},
				{
					name: 'подарков',
					count: '316'
				}
			],
			storyCaption: '2 истории',
			photosGallery: [
				'./img/gallery01.jpg',
				'./img/gallery02.jpg',
				'./img/gallery03.jpg',
				'./img/gallery04.jpg'
			],
			messagesNotificationsCount: 12,
			posts: [
				{
					name: 'лимонъ',
					time: 'сегодня в 9:41',
					text: 'Поехали покорять побережье и маленькие',
					attachment: {
						img: './img/img01.png',
						name: 'Don\'t buy iPhone X',
						views: '166 260 просмотров'
					},
					likesCount: 26,
					commentsCount: 4,
					repliesCount: 1,
					views: 1200,
					isLiked: true,
					friendsWhoLikesAvatars: ['./img/likes01.png'],
					friendsWhoLikesCaption: 'Понравилось Муртолу Лазвачеву'
				}
			]
		}
	},
	methods: {
		toggleLike(post) {
			post.isLiked = !post.isLiked;
			if (post.isLiked) {
				post.likesCount += 1;
				return;
			}
			post.likesCount -= 1;
		},
		getCount(count) {
			if (count >= 1000) {
				const num = parseFloat(count/1000);
				const rounded = Math.round(num * 10) / 10;

				return `${rounded}K`;
			}
			return count;

		},
		addNewPost() {
			this.screenSettings.posts.unshift({
				avatar: this.screenSettings.avatar,
				name: this.screenSettings.name,
				time: 'сегодня в 9:41',
				text: 'Some text',
				attachment: {
					img: './img/img01.png',
					name: 'Some title',
					views: '166 260 просмотров'
				},
				likesCount: 4129,
				commentsCount: 2145,
				repliesCount: 1672,
				views: '9.6K',
				isLiked: false,
				friendsWhoLikesAvatars: ['./img/avatar-default.png'],
				friendsWhoLikesCaption: 'Понравилось Кому-то'
			});
		}
	}
});