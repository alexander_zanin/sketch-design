import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import Settings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';


new Vue({
	el: '#app',
	components: {
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'settings': Settings,
		'loading-overlay': LoadingOverlay
	},
	mixins: [getscreenSettings],
	data: {
		databaseRef: 'ios-fb-new-post',
		outgoingMessage: '',
		fieldWasFocused: false,
		BIG_SYMBOLS_LENGTH: 82,
		HIDE_TOOLS_LENGTH: 190,

		screenSettings: {
			avatar: './img/avatar.png',
			name: 'UserName',
			placeholder: 'О чем вы думаете?'
		}
	},
	methods: {
		showKeyboard() {
			this.fieldWasFocused = true;
		}
	}
});