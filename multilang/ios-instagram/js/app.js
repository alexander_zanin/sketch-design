import Vue from '@common/vue';
import { showKeyboard } from '@common/mixins/show-keyboard';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';

new Vue({
	el: '#app',
	components: {
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'loading-overlay': LoadingOverlay
	},
	mixins: [getscreenSettings],
	data: {
		databaseRef: 'ios-instagram',
		showComments: false,
		outgoingMessage: '',
		screenSettings: {
			username: 'username',
			avatar: './img/profile-avatar.png',
			name: 'User Name',
			posts: [
				{
					id: Math.random(),
					photo: './img/img01.jpg',
					likes: 'Нравится этим людям и еще 150',
					date: '25 февраля',
					isLiked: false,
					comments: [
						{
							name: 'Some name',
							avatar: './img/post-avatar.png',
							text: 'Lorem ipsum.',
							time: '1h',
							likesCount: '1 like',
							isLiked: false
						}
					]
				}
			]
		},
	},
	methods: {
		toggleLike(post) {
			post.isLiked = !post.isLiked
		},
		addNewPost() {
			this.screenSettings.posts.push({
				photo: './img/img01.jpg',
				likes: 'Нравится этим людям и еще 150',
				date: '25 февраля',
				isLiked: false
			});
		},
		animationName() {
			if (this.showComments) {
				return 'slide';
			}
			return 'slide-reverse';
		},
		goToComments() {
			this.showComments = true;

			setTimeout(() => {
				this.$refs.textarea.$el.focus();
			}, 300);
		},
		sendComment() {
			this.post.comments.push({
				name: this.post.username,
				avatar: this.post.avatar,
				text: this.outgoingMessage,
				time: '1s',
				likesCount: '',
				isLiked: false,
			});

			this.outgoingMessage = '';
			this.$refs.textarea.$el.focus();
		}
	}
});