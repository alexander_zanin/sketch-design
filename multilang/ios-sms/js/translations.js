export const tr = {
	rus: {
		delivered: 'Доставлено',
		read: 'Прочитано'
	},

	en: {
		delivered: 'Delivered',
		read: 'Read'
	},

	fr: {
		delivered: 'Livré',
		read: 'Lire'
	}
}