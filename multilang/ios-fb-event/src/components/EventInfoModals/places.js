export const places = [
  {
    place: 'Москва',
    address: 'Москва'
  },
  {
    place: 'Хамовники',
    address: 'Москва'
  },
  {
    place: 'Храм Христа Спасителя',
    address: 'улица Волхонка 15'
  },
  {
    place: 'Здание 45',
    address: 'ул. Пушкинская 67'
  },
  {
    place: 'Здание 46',
    address: 'ул. Пушкинская 68'
  },
  {
    place: 'Здание 47',
    address: 'ул. Пушкинская 69'
  }
]
