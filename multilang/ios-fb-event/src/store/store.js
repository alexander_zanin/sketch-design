import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  modalIsVisible: false,
  showCreateEventPopup: false,
  datePopupIsVisible: false,
  placePopupIsVisible: false,
  eventScreenIsVisible: false,
  selectedEventType: '',
  eventName: '',
  eventTime: {
    date: new Date(1968, 0, 1),
    resultDate: 'Сегодня',
    hours: '17',
    minutes: '00'
  },
  eventPlace: {
    place: '',
    address: ''
  },
  screenSettings: {
    avatar: '',
    name: 'User Name',
    recommendedGallery: [
      {
        image: '',
        title: 'ЧТ, 28 МАР, 12-30',
        descr: 'Мирное шествие в поддержку бастующих мусорщиков'
      },
      {
        image: '',
        title: 'ЧТ, 28 МАР, 12-30',
        descr: 'Мирное шествие в поддержку бастующих мусорщиков'
      },
      {
        image: '',
        title: 'ЧТ, 28 МАР, 12-30',
        descr: 'Мирное шествие в поддержку бастующих мусорщиков'
      }
    ]
  }
}

const mutations = {
  showModal (state, propertyName) {
    state[propertyName] = true
  },
  hideModal (state, propertyName) {
    state[propertyName] = false
  },
  selectEventType (state, selectedType) {
    state.selectedEventType = selectedType
  },
  updateEventTime (state, newTimeObj) {
    state.eventTime = newTimeObj
  },
  selectPlace (state, place) {
    state.eventPlace = place
  },
  setEventName (state, name) {
    state.eventName = name
  },
  updateScreenSettings (state, settings) {
    state.screenSettings = settings
  }
}

const actions = {
  showModal: ({ commit }, propertyName) => commit('showModal', propertyName),
  hideModal: ({ commit }, propertyName) => commit('hideModal', propertyName),
  selectEventType: ({ commit }, selectedType) => {
    commit('selectEventType', selectedType)
    commit('hideModal', 'modalIsVisible')
    commit('showModal', 'showCreateEventPopup')
  },
  updateEventDate: ({ commit }, date) => {
    commit('updateEventDate', date)
  },
  updateEventTime: ({ commit }, time) => {
    commit('updateEventTime', time)
  },
  selectPlace ({ commit }, place) {
    commit('selectPlace', place)
  },
  setEventName ({ commit }, name) {
    commit('setEventName', name)
  },
  updateScreenSettings ({ commit }, settings) {
    commit('updateScreenSettings', settings)
  }
}

const getters = {
  modalIsVisible: state => state.modalIsVisible,
  selectedEventType: state => state.selectedEventType,
  showCreateEventPopup: state => state.showCreateEventPopup,
  datePopupIsVisible: state => state.datePopupIsVisible,
  placePopupIsVisible: state => state.placePopupIsVisible,
  eventScreenIsVisible: state => state.eventScreenIsVisible,
  screenSettings: state => state.screenSettings,
  eventName: state => state.eventName,
  eventTime: state => state.eventTime,
  eventTimeResult: state => `${state.eventTime.resultDate} в ${state.eventTime.hours}:${state.eventTime.minutes}`,
  eventPlace: state => state.eventPlace
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
