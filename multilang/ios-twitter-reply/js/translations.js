export const tr = {
	rus: {
		head: 'Твит',
		btn: 'Ответить',
		replyTo: 'В ответ',
		placeholder: 'Ваш ответ'
	},

	en: {
		head: 'Home',
		btn: 'Reply',
		replyTo: 'In response to',
		placeholder: 'Tweet your reply'
	},

	fr: {
		head: 'Accueil\n',
		btn: 'Répondre',
		replyTo: 'En réponse à',
		placeholder: 'Tweetez votre résponse.'
	}
}