import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import MessageLength from '@common/twitter/js/twitter-message-length';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'message-length' : MessageLength,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-twitter-reply',
		keyboardIsVisible: false,
		screenSettings: {
			currentLang: 'rus',
			tweetAuthorName: 'NewYorkTimes',
			tweetTime: '13м',
			tweetAuthorAvatar: './img/otherpostcomment copy bitmap.png',
			avatar: './img/otherpostcommentsvgid29.png',
			name: 'Stokely Carmichael',
			placeholder: 'Tweet your reply'
		}
	},
	methods: {
		showKeyboard() {
			this.keyboardIsVisible = true;
			this.toggleKeyboard();
		},
		toggleKeyboard() {
			const keyboard = this.$refs.keyboard.$el;
			const keyboardH = keyboard.offsetHeight;
			const inputBar = this.$refs.inputBar;

			if (this.keyboardIsVisible) {
				inputBar.style.marginBottom = `${keyboardH}px`;
			} else {
				inputBar.removeAttr('style');
			}
		}
	}
});