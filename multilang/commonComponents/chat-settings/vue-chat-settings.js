import Vue from '../vue';
import './css/chat-settings.css';
import VModal from 'vue-js-modal';
import vue2Dropzone from 'vue2-dropzone';
import 'vue2-dropzone/dist/vue2Dropzone.css';
import screenSettingsPresets from './componentns/vue-chat-settings-presets';
import screenSettingsSave from './componentns/vue-chats-settings-save';
import database from '../database/database';
import axios from 'axios';

const CLOUDINARY_URL = 'https://api.cloudinary.com/v1_1/df53nl3nc/image/upload';
const CLOUDINARY_UPLOAD_PRESET = 'ybqt5rvr';

Vue.use(VModal);

export default {
	name: 'chat-settings',
	components: {
		'vue-dropzone': vue2Dropzone,
		'chat-settings-presets': screenSettingsPresets,
		'chat-settings-save': screenSettingsSave
	},
	filters: {
		numberSuffix (number) {
			switch (number) {
				case 1:
					return `${number}st`
				case 2:
					return `${number}nd`
				case 3:
					return `${number}rd`
				default:
					return `${number}th`
			}
		}
	},
	props: {
		value: Object,
		databaseRef: String
	},
	data() {
		return {
			initialValue: null,
			inputs: {},
			dropzoneOptions: {
				url: 'https://httpbin.org/post',
				acceptedFiles: "image/*",
				thumbnailWidth: 250,
				dictDefaultMessage: 'drop photo or click here to upload',
				accept: (file, done) => {
					const formData = new FormData();
					const version = this.inputs.versionName ? this.inputs.versionName : 'default';
					// const date = new Date();
					// const today = `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`

					formData.append('file', file);
					formData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
					formData.append('folder', `${this.databaseRef} (vers: ${version}(${window.location.hostname}))`);

					axios({
						url: CLOUDINARY_URL,
						method: 'POST',
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						data: formData
					}).then((res) => {
						file.imageURL = res.data.secure_url;
						done();
					}).catch((err) => {
						console.error(err);
					});
				}
			},
			databaseVersionsRef: `${this.databaseRef}-versions`,
			currentVersionKey: null
		}
	},
	created() {
		this.updateInputsValue();
		this.initialValue = this.value;
	},
	watch: {
		value: {
			handler() {
				this.updateInputsValue();
			},
			deep: true
		}
	},
	methods: {
		getAvatar(file, propertyName = 'avatar', ...rest) {
			if (rest.length) {
				let res = this.inputs[propertyName];
				const lastRestElem = rest.length - 1;

				rest.forEach((value, index) => {
					if (index === lastRestElem) return;
					res = res[value]
				});

				// res[rest[lastRestElem]] = file.dataURL;
				res[rest[lastRestElem]] = file.imageURL;
			} else {
				// this.inputs[propertyName] = file.dataURL;
				this.inputs[propertyName] = file.imageURL;
			}

			this.emitInput();
		},
		// sendingEvent(file, xhr, formData) {
		// 	formData.append('file', file);
		// 	formData.append('upload_preset', 'ybqt5rvr');
		// },
		emitInput() {
			const inputs = {};

			for (const key in this.inputs) {
				inputs[key] = this.inputs[key];
			}
			this.$emit('input', inputs);
		},
		updateInputsValue() {
			// for (const key in this.value) {
			// 	this.$set(this.inputs, key, this.value[key]);
			// }
			this.inputs = JSON.parse(JSON.stringify(this.value));
		},
		addElemToArray(obj, targetArr, elem) {

			if (!obj[targetArr]) obj[targetArr] = [];

			const array = obj[targetArr];

			if (elem) {
				array.push(elem);
			} else {
				array.push(array[0]);
			}

			this.emitInput();
		},
		removeElemFromArray(array) {
			array.pop();
			this.emitInput();
		},
		selectVersion(version, versionKey) {
			this.inputs = version;
			this.currentVersionKey = versionKey;
			this.emitInput();
		},
		removeVersion(versionKey) {
			if (this.currentVersionKey === versionKey) {
				this.selectDefault();
			}

			database.ref(this.databaseVersionsRef).child(versionKey).remove((error) => {
				if (error) console.log('Unable to delete', error);
			});
		},
		addNameVersion(versionName) {
			this.inputs.versionName = versionName;
		},
		setCreatedVersion(inputs) {
			this.inputs = inputs;
		},
		selectDefault() {
			database.ref(this.databaseRef).once('value', (snapshot) => {
				this.currentVersionKey = null;

				if (snapshot.val()) {
					this.inputs = snapshot.val();
				} else {
					this.inputs = this.initialValue;
					this.$delete(this.inputs, 'versionName');
				}

				this.emitInput();
			});
		}
	},
	template: '#chat-settings-template'
}