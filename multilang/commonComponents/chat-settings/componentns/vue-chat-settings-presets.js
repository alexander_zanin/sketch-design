import Vue from '../../vue';
import VModal from 'vue-js-modal';
import database from '../../database/database';

Vue.use(VModal);

export default {
	name: 'chat-settings-presets',
	props: {
		inputs: Object,
		databaseVersionsRef: String
	},
	data() {
		return {
			versions: {},
			versionForRemoveKey: null
		}
	},
	created() {
		this.fetchVersions();
	},
	methods: {
		fetchVersions() {
			database.ref(this.databaseVersionsRef).on('value', (snapshot) => {
				if (!snapshot.val()) {
					this.versions = {};
					return;
				}
				this.versions = snapshot.val();
			});
		},
		selectVersion(version, versionKey) {
			this.$emit('version-was-selected', version, versionKey);
		},
		showModal(versionKey) {
			this.versionForRemoveKey = versionKey;
			this.$modal.show('chat-settings-question');
		},
		hideModal() {
			this.$emit('version-remove-was-clicked', this.versionForRemoveKey)
			this.$modal.hide('chat-settings-question');
		}
	},
	template: `
	<div class="chat-settings-presets">
		<p class="chat-settings-presets__block">
			<strong>Current version:</strong>
			{{ inputs.versionName ? inputs.versionName : 'default' }}
		</p>
		<div v-if="Object.keys(versions).length" class="chat-settings-presets__block">
			<h4 class="chat-settings-presets__title">
				List of versions/presets:
			</h4>
			<ul class="chat-settings-presets__list">
				<li v-for="(version, key) in versions"
					class="chat-settings-presets__item">
					<a href="#"
					   class="chat-settings-presets__link"
					   :class="{ 'chat-settings-presets__link--active' : inputs.versionName === version.versionName }"
					   @click.prevent="selectVersion(version, key)">
						{{ version.versionName }}
					</a>
					<button @click="showModal(key)" class="chat-settings-presets__remove">❌</button>
				</li>
			</ul>
		</div>
		<div v-else class="chat-settings-presets__block">
			There are no versions/presets yet...
		</div>
		<div class="chat-settings-presets__item">
			<a 
				href="#"
				:class="{ 'chat-settings-presets__link--active' : !inputs.versionName }"
				@click.prevent="$emit('select-default-was-clicked')"
				class="chat-settings-presets__link">
				Default version
			</a>
		</div>
		
		<modal
				class="chat-settings-question-popup"
				name="chat-settings-question"
				:width="1000"
				:height="'auto'" 
				@before-close="versionForRemoveKey = null">
			<p class="chat-settings-question-popup__text">
				Are you sure you want to remove this version/preset?
			</p>
			<div class="chat-settings-question-popup__row">
				<button class="button button--danger" @click="hideModal">
					Remove
				</button>
				<button class="button" @click="$modal.hide('chat-settings-question')">
					Cancel
				</button>
			</div>
		</modal>
	</div>
	`
}