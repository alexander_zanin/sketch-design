import database from '../../database/database';

export default {
	name: 'chat-settings-save',
	props: {
		inputs: Object,
		databaseRef: String,
		databaseVersionsRef: String,
		versionKey: String
	},
	data() {
		return {
			dataSaving: false,
			newVersionSaving: false,
			savingWasFailed: false,
			versionName: '',
		}
	},
	methods: {
		updateSettings() {
			this.setToDatabase(database.ref(this.databaseVersionsRef).child(this.versionKey));
		},
		saveSettings() {
			this.setToDatabase(database.ref(this.databaseRef));
		},
		setToDatabase(ref) {
			this.dataSaving = true;

			ref.set(this.inputs, (error) => {
				this.onSave(error, () => {
					this.dataSaving = false;
				});
			});
		},
		createNewVersion() {
			this.newVersionSaving = true;

			const versionsRef = database.ref(this.databaseVersionsRef);
			const versionRef = versionsRef.push();

			// this.inputs.versionName = this.versionName;
			this.$emit('version-name-was-added', this.versionName);
			const data = Object.assign({}, this.inputs);

			versionRef.set(data, (error) => {
				this.onSave(error, () => {
					this.versionName = '';
					this.newVersionSaving = false;
				});
			});

			versionsRef.limitToLast(1).on('child_added', (snapshot) => {
				this.$emit('new-version-was-added', snapshot.val());
				// this.inputs = snapshot.val();
			});
		},
		onSave(error, callback) {
			if (error) {
				// The write failed...
				console.log('Saving data to database was failed', error);
				this.savingWasFailed = true;
			} else {
				// Data saved successfully!
				this.$modal.hide('chat-settings');
			}
			callback();
		},
	},
	template: `
	<div class="chat-settings__row">
		<div class="chat-settings__row">
			<button v-if="inputs.versionName"
					@click="updateSettings"
					:disabled="dataSaving"
					:class="{ 'button--loading': dataSaving }"
					class="button"
					type="submit">
				Update settings
			</button>
			<button v-else
					@click="saveSettings"
					:disabled="dataSaving"
					:class="{ 'button--loading': dataSaving }"
					class="button"
					type="submit">
				Save as default
			</button>
		</div>
		<h3 class="chat-settings__title">
			Create new version:
		</h3>
		<div class="chat-settings__row">
			<label class="chat-settings__label">
				Version name:
			</label>
			<input
					v-model="versionName"
					type="text"
					class="chat-settings__input">
		</div>
		<div class="chat-settings__row">
			<button
					@click="createNewVersion"
					:disabled="!versionName.length || newVersionSaving"
					:class="{ 'button--loading': newVersionSaving }"
					class="button"
					type="submit">
				Create
			</button>
		</div>


		<div v-if="savingWasFailed" class="chat-settings__row">
			<p class="chat-settings__error">
				Something went wrong. Please, try later.
			</p>
		</div>
	</div>
	`
}