import './css/switch-lang.css'

export default {
	name: 'switch-lang',
	props: {
		value: String
	},
	data() {
		return {
			// selectedLang: 'rus'
		}
	},
	methods: {
		emitInput(e) {
			this.$emit('input', e.target.value);
		}
	},
	template: `
	<div class="switch-lang-holder">
		<h4 class="switch-lang-holder__title">Language:</h4>
		<select class="switch-lang-select" :value="value" @change="emitInput($event)">
			<option value="rus" class="switch-lang-select__option">RUS</option>
			<option value="en" class="switch-lang-select__option">ENG</option>
			<option value="fr" class="switch-lang-select__option">FR</option>
		</select>
	</div>
	`
}