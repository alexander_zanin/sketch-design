import '../vue';
import './css/outgoing-message.css';
import eventBus from '../tools/vue-event-bus';
import autosize from 'autosize';

export default {
	name: 'outgoing-message',
	props: {
		value: String,
	},
	data() {
		return {
			passKey: false,
			increaseFontSize: false,
			capitalize: null
		}
	},
	watch: {
		increaseFontSize () {
			this.updateAutosize();
		},
		value() {
			const EMOJI_LENGTH = 2;
			if (this.increaseFontSize && this.value.length > EMOJI_LENGTH) {
				this.increaseFontSize = false;
			}

			if (this.value.length) return;

			this.increaseFontSize = false;
			this.updateAutosize();
		}
	},
	created() {
		eventBus.$on('keyboard-was-pressed', this.inputText);

		eventBus.$on('keyboard-was-switched', this.passKeyProperty);

		eventBus.$on('symbol-was-selected', this.addSymbol);
		
		eventBus.$on('capitalize', (capitalize) => {
			this.capitalize = capitalize;
		});

		eventBus.$on('outgoing-message-was-sent', () => {
			this.$refs.textarea.focus();
		});
	},
	mounted() {
		const textarea = this.$refs.textarea;

		autosize(textarea);

		textarea.addEventListener('autosize:resized', (e) => {
			eventBus.$emit('textarea-height-was-updated', e.target.offsetHeight);
		});

	},
	destroyed() {
		[
			'keyboard-was-pressed',
			'keyboard-was-switched',
			'symbol-was-selected',
			'capitalize',
			'outgoing-message-was-sent'
		].forEach((item) => {
			eventBus.$off(item);
		});
	},
	methods: {
		getKeyCode(e) {
			const DELETE_KEY = 8;

			// for keys combinations (like : ; ' ! ? etc)
			if (this.passKey) {
				this.emitKeydown(e.key);

				if (e.keyCode === DELETE_KEY) {
					this.emitKeydown(e.keyCode);
				}
			} else {
				this.emitKeydown(e.keyCode);

				if (e.key === '.' || e.key ===',') {
					this.emitKeydown(e.key);
				}
			}


			// this.resetKeyCode();
		},
		/*resetKeyCode() {
			this.$nextTick(() => {
				this.emitKeydown(null);
			});
		},*/
		deleteButtonClick(key) {
			const DELETE_KEY = 8;

			if (key.keyID === DELETE_KEY) {
				this.$emit('input', this.value.slice(0, -1));
			}
		},
		inputText(key) {
			let passedValue = this.value;
			this.$emit('input', passedValue += key.value ? key.value : '');
			this.$refs.textarea.focus();

			if (!key.symbolsWasPassed) {
				this.emitKeydown(key.keyID ? key.keyID : key.value);
			}

			this.updateAutosize();
			this.deleteButtonClick(key);
			// this.resetKeyCode();
		},
		passKeyProperty(charsIsActive) {
			// pass key property instead of keyCode property
			this.passKey = !charsIsActive;
			this.$refs.textarea.focus();
		},
		addSymbol(symbol) {
			let passedValue = this.value;
			this.$emit('input', passedValue += symbol);
			this.$refs.textarea.focus();

			if (this.value.length) {
				this.increaseFontSize = false;
				return;
			}
			this.increaseFontSize = true;
			this.updateAutosize();
		},
		updateAutosize() {
			this.$nextTick(() => {
				autosize.update(this.$refs.textarea);
			});
		},
		emitKeydown(value) {
			eventBus.$emit('keydown', value);
		},
		emitInput(e) {
			let value = e.target.value;

			if (this.capitalize) {
				const lastCapitilized = value[value.length - 1].toUpperCase();
				value = value.slice(0, -1) + lastCapitilized;
			}

			this.$emit('input', value);
		}
	},
	template: `
	<textarea
			class="message-text"
			:class="{ 'message-text--fz-big' : increaseFontSize }"
			ref="textarea"
			:value="value"
			@input="emitInput"
			@keydown="getKeyCode($event)"
	></textarea>`
}