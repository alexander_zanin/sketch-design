import '../vue';
import './css/incoming-message.css';
import eventBus from '../tools/vue-event-bus';
import vue2Dropzone from 'vue2-dropzone';
import 'vue2-dropzone/dist/vue2Dropzone.css';

export default {
	name: 'incoming-message',
	components: {
		'vue-dropzone': vue2Dropzone
	},
	props: {
		value: Boolean,
		stickers: Array,
		messagesWithTime: Boolean
	},
	data() {
		return {
			incomingMessageText: '',
			typingTimeout: null,
			groupChat: {
				participantsAmount: null,
				participantsAmountWasPassed: false,
				notificationMessageText: ''
			},
			interlocutor: {
				name: '',
				isTyping: false,
				isOnline: false,
				avatar: ''
			},
			emoji: {
				single: false,
				sendAsOutgoing: false,
				symbols: ['🤐', '🤔', '😔', '😊', '😫', '☹️', '😱', '😅', '😒', '🤣', '😠', '😤', '😡', '🙏', '🙏🏻', '🙏🏼', '🙏🏽', '🙏🏾', '🙏🏿']
			},
			dropzoneOptions: {
				url: 'https://httpbin.org/post',
				maxFiles: 1,
				thumbnailWidth: 150,
				dictDefaultMessage: 'drop photo or click here to upload',
				addRemoveLinks: true,
				headers: {"My-Awesome-Header": "header value"}
			}
		}
	},
	watch: {
		incomingMessageText() {
			const EMOJI_lENGTH = 2;
			if (this.incomingMessageText.length >EMOJI_lENGTH) {
				this.emoji.single = false;
			}
		}
	},
	methods: {
		getAvatar(file) {
			this.interlocutor.avatar = file.dataURL;
		},
		removeAvatar() {
			this.interlocutor.avatar = '';
		},
		sendMessage() {
			const newLines = /(?:\r\n|\r|\n)/g;
			this.incomingMessageText = this.incomingMessageText.replace(newLines, '<br>');

			const message = {
				isIncoming: true,
				content: this.incomingMessageText
			};

			if (this.messagesWithTime) {
				message.customTime = '12:00 AM';
			}

			if (this.emoji.single) {
				message.increaseFontSize = true;
			}

			if (this.value && this.interlocutor.avatar) {
				message.avatar = this.interlocutor.avatar;
			}

			this.emitFormSubmit(message);
			this.incomingMessageText = '';
		},
		addSymbol(symbol) {
			if (this.emoji.sendAsOutgoing) {
				eventBus.$emit('symbol-was-selected', symbol);
				return;
			}

			this.emoji.single = true;
			this.incomingMessageText += symbol;
			this.$refs.textarea.focus();
		},
		sendSticker(src) {
			const message = {
				isIncoming: true,
				isSticker: true,
				src
			};
			this.emitFormSubmit(message);
		},
		sendBigBubble(isIncoming) {
			const message = {
				isIncoming,
				content: `<div class="rectangle"></div>`
			};
			this.emitFormSubmit(message);
		},
		addAuthor(message) {
			if (this.value && this.interlocutor.name) {
				message.authorName = this.interlocutor.name;
			}
			return message;
		},
		getTypingStatus() {
			if (this.typingTimeout !== undefined) clearTimeout(this.typingTimeout);
			this.interlocutor.isTyping = true;
			this.typingTimeout = setTimeout(this.stopTyping, 1000);
			this.emitTyping();
		},
		stopTyping() {
			this.interlocutor.isTyping = false;
			this.emitTyping();
		},
		setOnlineStatus() {
			this.interlocutor.isOnline = true;
			this.$emit('field-was-focused', this.interlocutor.isOnline);
		},
		/*removeOnlineStatus() {
			this.interlocutor.isOnline = false;
			this.interlocutor.isTyping = false;
			this.$emit('field-was-unfocused', this.interlocutor.isOnline);
			this.emitTyping();
		},*/
		emitTyping() {
			this.$emit('message-was-entered', this.interlocutor.isTyping, this.interlocutor.name);
		},
		emitFormSubmit(message) {
			eventBus.$emit('incoming-message-was-sent', this.addAuthor(message));
		},
		setNumberOfParticipants() {
			this.groupChat.participantsAmountWasPassed = true;
			this.$emit('participants-amount-was-added', this.groupChat.participantsAmount);
		},
		sendNotificationMessage() {
			const message = {
				isNotification: true,
				content: this.groupChat.notificationMessageText
			};
			this.emitFormSubmit(message);
		}
	},
	template: '#incoming-message-template'
}