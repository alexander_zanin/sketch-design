import '../vue';
import './css/messages-list.css';
import easyScroll from 'easy-scroll';
// import zenscroll from 'zenscroll';
import eventBus from '../tools/vue-event-bus';


export default {
	name: 'messages-list',
	props: {
		value: Array, // if we need two-way data binding for messages
		doNotUpdateScrollAfterLoad: Boolean,
		doNotUpdateScrollAfterSend: Boolean,
		interlocutor: Object,
		isGroupChat: Boolean,
		screenSettings: Object,
		defaultMessages: Array,
		additionalSettings: Object // for different screens with various styles and elements
	},
	data() {
		return {
			messages: []
		}
	},
	watch: {
		messages() {
			if (this.doNotUpdateScrollAfterSend) return;

			this.$nextTick(this.scrollListToBottom);
		},
		interlocutor: {
			handler() {
				this.checkIsInterlocutorTyping();
			},
			deep: true
		},
		defaultMessages() {
			this.messages = this.defaultMessages;
		},
		value() {
			if (!this.value) return;
			this.messages = JSON.parse(JSON.stringify(this.value))
		}
	},
	created() {
		if (this.defaultMessages) {
			this.messages = this.defaultMessages;
		}

		if (!this.doNotUpdateScrollAfterLoad) {
			eventBus.$on('textarea-height-was-updated', () => {
				this.$nextTick(this.scrollListToBottom);
			});

			eventBus.$on('keyboard-was-shown', () => {
				const CSS_KEYBOARD_ANIMATION_DURATION = 350;
				setTimeout(this.scrollListToBottom, CSS_KEYBOARD_ANIMATION_DURATION);
			});
		}

		eventBus.$on('incoming-message-was-sent', this.getMessage);

		eventBus.$on('outgoing-message-was-sent', this.getMessage);

		eventBus.$on('group-chat-was-created', (isGroupChat) => {
			this.isGroupChat = isGroupChat;
		});

		this.checkIsInterlocutorTyping();
	},
	destroyed() {
		[
			'incoming-message-was-sent',
			'outgoing-message-was-sent',
			'group-chat-was-created',
		].forEach((item) => {
			eventBus.$off(item);
		});
	},
	methods: {
		scrollListToBottom() {
			const messagesHolder = this.$refs.messagesHolder;
			// messagesHolder.setAttribute('style', 'scroll-behavior: smooth;')
			easyScroll({
				'scrollableDomEle': messagesHolder,
				'direction': 'bottom',
				'duration': 200,
				'easingPreset': 'linear',
				onAnimationCompleteCallback() {
					messagesHolder.scrollTop = messagesHolder.scrollHeight;
				}
			});
			// messagesHolder.scrollTop = messagesHolder.scrollHeight;
			// const scrollable = zenscroll.createScroller(messagesHolder, 400);
			// scrollable.toY(messagesHolder.scrollHeight);
		},
		getMessage(message) {
			this.checkIsInterlocutorTyping();

			this.$nextTick(() => {
				this.checkMessageOrder(message);

				message.time = new Date().getTime();
				// this.checkMessageDate(message);

				this.messages.push(message);

				this.$emit('input', this.messages);
			})
		},
		checkMessageOrder(message) {
			const lastElem = this.messages[this.messages.length - 1];

			message.isLastFromAuthor = true;

			if (!lastElem) {
				message.isFirstFromAuthor = true;
				return;
			}

			const authorNameIsExist = !!lastElem.authorName;

			if (authorNameIsExist && lastElem.authorName === message.authorName) { // for group chats
				message.isNextFromAuthor = true;
				delete lastElem.isLastFromAuthor;
			} else if (!authorNameIsExist && lastElem.isIncoming === message.isIncoming) { // for regular chats
				delete lastElem.isLastFromAuthor;
			}
		},
		/*checkMessageDate(message) {
			const lastElem = this.messages[this.messages.length - 1];

			if (!lastElem || this.dateWithoutTime(lastElem.time) !== this.dateWithoutTime(message.time)) {
				message.historyMessage = true;
				if (this.dateWithoutTime(message.time) === this.dateWithoutTime(new Date())) {
					message.isToday = true;
				}
			}
		},*/
		/*dateWithoutTime(time) {
			return new Date(time).setHours(0, 0, 0, 0);
		},*/
		getFormattedTime(time) {
			const hours = this.addZeroBefore(new Date(time).getHours());
			const minutes = this.addZeroBefore(new Date(time).getMinutes());
			return `${hours}:${minutes}`;
		},
		addZeroBefore(number) {
			return number < 10 ? `0${number}` : number;
		},
		checkIsInterlocutorTyping() {
			if (!this.interlocutor) return;

			if (this.interlocutor.isTyping) {
				this.messages.push({
					isTyping: true,
					isIncoming: true,
					content: ''
				});
				return;
			}

			this.messages = this.messages.filter(message => !message.isTyping);
		}
	},
	template: '#messages-list-template'
}