import '../vue';
import './css/keyboard.css';
import IosKeyboardKey from './components/vue-ios-keyboard-key';
import { chars, symbols, symbolsControls, mainControls } from './keys/keys';
import eventBus from '../tools/vue-event-bus';

export default {
	name: 'ios-keyboard',
	components: {
		'ios-keyboard-key': IosKeyboardKey
	},
	props: {
		lang: String
	},
	data() {
		return {
			pressedKeyCode: null,
			charsIsActive: true,
			capitalize: true,
			chars,
			symbols,
			symbolsControls,
			mainControls
		}
	},
	created() {
		this.selectSymbols();

		eventBus.$on('keydown', this.setPressedKeyCode);

		eventBus.$emit('capitalize', this.capitalize);

		eventBus.$on('outgoing-message-was-sent', this.capitalizeKeyboard);
	},
	destroyed() {
		eventBus.$off('keydown', this.setPressedKeyCode);
		eventBus.$off('outgoing-message-was-sent', this.capitalizeKeyboard);
	},
	watch: {
		pressedKeyCode() {
			if (!this.pressedKeyCode) return;
			this.checkKeyIsShift();
		},
		capitalize() {
			eventBus.$emit('capitalize', this.capitalize);
		},
		lang() {
			this.selectSymbols();
		}
	},
	methods: {
		setPressedKeyCode(keyCode) {
			this.pressedKeyCode = keyCode;
		},
		capitalizeKeyboard() {
			this.capitalize = true;
		},
		selectSymbols() {
			switch (this.lang) {
				case 'en':
					this.setKeysData(require('./keys/keys-en'));
					break;
				case 'fr':
					this.setKeysData(require('./keys/keys-fr'));
					break;
				default:
					this.setKeysData(require('./keys/keys'));
					break;
			}
		},
		setKeysData(keys) {
			for (const key in keys) {
				this[key] = keys[key];
			}
		},
		clickKey(key) {
			if (this.capitalize && key.value) {
				this.capitalize = false;

				const keyCapitalized = Object.assign({}, key);
				keyCapitalized.value = keyCapitalized.value.toUpperCase();

				eventBus.$emit('keyboard-was-pressed', keyCapitalized);
				return;
			}

			eventBus.$emit('keyboard-was-pressed', key);
		},
		findBy(arr, prop, value) {
			return arr.find(key => key[prop] === value)
		},
		switchKeyboard() {
			this.charsIsActive = !this.charsIsActive;
			eventBus.$emit('keyboard-was-switched', this.charsIsActive);
		},
		checkKeyIsShift() {
			const SHIFT_CODE = 16;

			if (this.pressedKeyCode === SHIFT_CODE && this.capitalize) {
				this.capitalize = false;
				return;
			}

			this.capitalize = this.pressedKeyCode === SHIFT_CODE;
		}
	},
	template: `
	<div class="keyboard-holder" :class="{ [lang] : lang }">
		<div class="keyboard">

			<div v-if="charsIsActive" class="chars-board" :class="{ 'capitalize' : capitalize }">
				<ios-keyboard-key v-for="key in chars"
								  :keyObj="key"
								  :capitalize="capitalize"
								  :key="key.value"
								  @keyboard-was-pressed="clickKey"></ios-keyboard-key>
			</div>
			<div v-else class="symbols-holder">
				<div class="symbols-board">
					<ios-keyboard-key v-for="key in symbols"
									  :keyObj="key"
									  :key="key.value"
									  @keyboard-was-pressed="clickKey"></ios-keyboard-key>
				</div>
				<div class="symbols-controls">
					<ios-keyboard-key v-for="key in symbolsControls"
									  :class="{ 'big-control': key.isControl }"
									  :keyObj="key"
									  :key="key.value"
									  @keyboard-was-pressed="clickKey"></ios-keyboard-key>
				</div>
			</div>

			<div class="main-controls">
				<div class="key switch-boards gray"
					 @click="switchKeyboard"
					 :class="{ 'fz-s' : charsIsActive, 'fz-xs' : !charsIsActive }">
					{{ charsIsActive ? '123' : 'АБВ' }}
				</div>
				<div class="key lang gray">
					<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1OCIgaGVpZ2h0PSI1OCIgdmlld0JveD0iMCAwIDU4IDU4Ij4KICAgIDxnIGZpbGw9IiMwMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNOS4xNSA1MC4wMTNjLjE5My0uMjE5LjYxLS42MzcgMS4yNDItMS4xODlhMjYuNDMxIDI2LjQzMSAwIDAgMSAzLjgyOS0yLjc1N2M0LjE2Ni0yLjQ4NCA5LjAzLTMuOTg4IDE0LjU1OS00LjAyNiA1LjU2NS0uMDM5IDEwLjU1OSAxLjU0IDE0LjkxIDQuMTk4YTI4LjQ1IDI4LjQ1IDAgMCAxIDMuOTM4IDIuODg1Yy4zODguMzQuNzIuNjU0Ljk5OS45MzQuMTYuMTYuMjYzLjI3LjMwNy4zMmwyLjE1Mi0xLjkxNGMtLjI4Mi0uMzE4LS44MDQtLjg0Mi0xLjU1Ny0xLjUwM2EzMS4zMTUgMzEuMzE1IDAgMCAwLTQuMzM3LTMuMThjLTQuNzg0LTIuOTIyLTEwLjI5MS00LjY2My0xNi40MzItNC42Mi02LjA3MS4wNDItMTEuNDI4IDEuNjk4LTE2LjAxNCA0LjQzMmEyOS4yOTggMjkuMjk4IDAgMCAwLTQuMjQ2IDMuMDZjLS43MzIuNjM4LTEuMjM3IDEuMTQ1LTEuNTEgMS40NTVsMi4xNiAxLjkwNXpNNi45OSAxMS42NzJjLjI3My4zMS43NzguODE3IDEuNTEgMS40NTVhMjkuMjk4IDI5LjI5OCAwIDAgMCA0LjI0NiAzLjA2YzQuNTg2IDIuNzM0IDkuOTQzIDQuMzkxIDE2LjAxNCA0LjQzMyA2LjE0MS4wNDIgMTEuNjQ4LTEuNjk4IDE2LjQzMi00LjYyYTMxLjMxNSAzMS4zMTUgMCAwIDAgNC4zMzctMy4xOGMuNzUzLS42NjEgMS4yNzUtMS4xODUgMS41NTctMS41MDNsLTIuMTUyLTEuOTE0YTkuMDU0IDkuMDU0IDAgMCAxLS4zMDcuMzJjLS4yNzguMjgtLjYxMS41OTMtLjk5OS45MzRhMjguNDUgMjguNDUgMCAwIDEtMy45MzggMi44ODVjLTQuMzUxIDIuNjU4LTkuMzQ1IDQuMjM2LTE0LjkxIDQuMTk4LTUuNTI5LS4wMzgtMTAuMzkzLTEuNTQyLTE0LjU2LTQuMDI2YTI2LjQzMSAyNi40MzEgMCAwIDEtMy44MjgtMi43NTdjLS42MzItLjU1Mi0xLjA0OS0uOTctMS4yNDItMS4xOWwtMi4xNiAxLjkwNXoiLz4KICAgICAgICA8cGF0aCBkPSJNMjcuNiAzLjE2aDIuODh2NTMuMjhIMjcuNnoiLz4KICAgICAgICA8cGF0aCBkPSJNLjI0IDI3LjY0aDU3LjZ2Mi44OEguMjR6Ii8+CiAgICAgICAgPHBhdGggZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMjkuNzk0IDU1QzM1LjgyMyA1NSA0MiA0My4zODYgNDIgMjkuMDhjMC0xNC4zMDYtNi4xNzctMjUuOTItMTIuMjA2LTI1LjkyLTcuMzU4IDAtMTMuNzE0IDExLjE4OC0xMy43MTQgMjUuOTJDMTYuMDggNDMuODEyIDIyLjQzNiA1NSAyOS43OTQgNTV6bTAgMi44OGMtOS41MDIgMC0xNi41OTQtMTIuODk0LTE2LjU5NC0yOC44UzIwLjI5Mi4yOCAyOS43OTQuMjhjNy45OTQgMCAxNS4wODYgMTIuODk0IDE1LjA4NiAyOC44cy03LjA5MiAyOC44LTE1LjA4NiAyOC44eiIvPgogICAgICAgIDxwYXRoIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTI5LjA0IDU1YzE0LjMxNSAwIDI1LjkyLTExLjYwNSAyNS45Mi0yNS45MiAwLTE0LjMxNS0xMS42MDUtMjUuOTItMjUuOTItMjUuOTItMTQuMzE1IDAtMjUuOTIgMTEuNjA1LTI1LjkyIDI1LjkyQzMuMTIgNDMuMzk1IDE0LjcyNSA1NSAyOS4wNCA1NXptMCAyLjg4Yy0xNS45MDYgMC0yOC44LTEyLjg5NC0yOC44LTI4LjhTMTMuMTM0LjI4IDI5LjA0LjI4czI4LjggMTIuODk0IDI4LjggMjguOC0xMi44OTQgMjguOC0yOC44IDI4Ljh6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=">
				</div>
				<div class="key mic">
					<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSI1OCIgdmlld0JveD0iMCAwIDMyIDU4Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjMiIGQ9Ik0zMC40NiAyMS45OTZsLTI4LjY4LS4wODJ2OS45MTVjMCA3LjEzNCA1Ljc3NSAxMi45MTEgMTIuOTAzIDEyLjkxMWgyLjg3NGM3LjEyNiAwIDEyLjkwMy01Ljc3NyAxMi45MDMtMTIuOTAzdi05Ljg0MXoiLz4KICAgICAgICA8cGF0aCBmaWxsPSIjMDAwIiBmaWxsLXJ1bGU9Im5vbnplcm8iIGQ9Ik04LjkyIDEwLjIzdjIwLjE4YzAgMy45NyAzLjIyMyA3LjE5IDcuMiA3LjE5IDMuOTcgMCA3LjItMy4yMjQgNy4yLTcuMTlWMTAuMjNjMC0zLjk3LTMuMjIzLTcuMTktNy4yLTcuMTktMy45NyAwLTcuMiAzLjIyNC03LjIgNy4xOXptLTIuODggMEM2LjA0IDQuNjcgMTAuNTY0LjE2IDE2LjEyLjE2YzUuNTY3IDAgMTAuMDggNC41MDggMTAuMDggMTAuMDd2MjAuMThjMCA1LjU2MS00LjUyNCAxMC4wNy0xMC4wOCAxMC4wNy01LjU2NyAwLTEwLjA4LTQuNTA4LTEwLjA4LTEwLjA3VjEwLjIzek0xNC42OCA0NC44djExLjM0MWgyLjg4VjQ0Ljh6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNNi4wNCA1Ny43NkgyNi4ydi0yLjg4SDYuMDR6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=">
				</div>
				<ios-keyboard-key v-for="key in mainControls"
								  :class="key.classes"
								  :keyObj="key"
								  :key="key.value"
								  @keyboard-was-pressed="clickKey"></ios-keyboard-key>
			</div>

		</div>
	</div>
	`
}