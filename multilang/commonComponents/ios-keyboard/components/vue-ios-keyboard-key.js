import '../../vue';
import eventBus from '../../tools/vue-event-bus';

export default {
	name: 'ios-keyboard-key',
	template: `
	<div
			:class="addClass()"
			class="key"
			:data-active="keyObj.value"
			@mouseup="removeTimer"
			@mousedown="listenKeyPress($event, keyObj)"
			@click="clickKey(keyObj)">

		<template v-if="keyObj.alias">
			{{ keyObj.alias }}
		</template>
		<template v-else-if="keyObj.value">
			{{ keyObj.value }}

			<div class="key-grip" v-if="longPressWasDetected"></div>
			<div
					v-if="longPressWasDetected"
					:style="keyObj.symbolsPopupStyles"
					class="key-additional-symbols">
				<span
						v-for="symbol in keyObj.additionalSymbols"
						@mouseup="selectSymbol(symbol, keyObj)"
						class="additional-key">
					{{ symbol }}
				</span>
			</div>
		</template>
		<template v-else>
			<img v-if="controlActiveImg()" :src="keyObj.imgActive" alt="control">
			<img v-else :src="keyObj.img" alt="control">
		</template>

	</div>
	`,
	props: {
		keyObj: Object,
		capitalize: Boolean
	},
	data() {
		return {
			pressTimer: null,
			longPressWasDetected: false,
			isActiveNow: false
		}
	},
	computed: {
		shiftButtonIsActive() {
			const SHIFT_KEY_CODE = 16;

			return this.keyObj.keyID === SHIFT_KEY_CODE && this.capitalize;
		}
	},
	created() {
		eventBus.$on('keydown', this.activateSelectedButton);
	},
	destroyed() {
		eventBus.$off('keydown', this.activateSelectedButton);
	},
	methods: {
		activateSelectedButton(keyCode) {
			if (this.keyObj.keyID && this.keyObj.keyID === keyCode) {
				this.makeButtonActive();
			} else if (this.keyObj.value === keyCode) {
				this.makeButtonActive();
			}
		},
		makeButtonActive() {
			this.isActiveNow = true;

			setTimeout(() => {
				this.isActiveNow = false;
			}, 100);
		},
		removeTimer() {
			clearTimeout(this.pressTimer);
			this.$nextTick(() => {
				this.longPressWasDetected = false;
			});
		},
		listenKeyPress(event, key) {
			this.pressTimer = window.setTimeout(() => {
				if (!key.additionalSymbols) return;

				this.longPressWasDetected = true;

			}, 300);
		},
		selectSymbol(symbol, keyObj) {
			this.$emit('keyboard-was-pressed', {
				additionalSymbols: keyObj.additionalSymbols,
				keyID: keyObj.keyID,
				value: symbol,
				symbolsWasPassed: true
			});
		},
		clickKey(key) {
			if (this.longPressWasDetected) return;

			this.$emit('keyboard-was-pressed', key);
		},
		controlActiveImg() {
			return this.isActiveNow && this.keyObj.imgActive || this.shiftButtonIsActive;
		},
		addClass() {
			const SPACE_KEY_CODE = 32;

			return {
				'gray': this.keyObj.isControl && this.keyObj.keyID !== SPACE_KEY_CODE,
				'long-pressed': this.longPressWasDetected,
				'is-pressed': this.keyObj.isControl && this.isActiveNow || this.shiftButtonIsActive,
				'is-scaled': !this.keyObj.isControl && this.isActiveNow
			}
		}
	}
}