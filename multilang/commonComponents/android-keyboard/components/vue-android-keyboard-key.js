import '../../vue';
import eventBus from '../../tools/vue-event-bus';

export default {
	name: 'android-keyboard-key',
	template: `
	<div
			:class="addClass()"
			class="key"
			:data-active="keyObj.value"
			@mouseup="removeTimer"
			@mousedown="listenKeyPress($event, keyObj)"
			@click="clickKey(keyObj)"
			:data-caption="keyObj.caption">

		<template v-if="keyObj.alias">
			{{ keyObj.alias }}
		</template>
		<template v-else-if="keyObj.img">
			<img  :src="keyObj.img" alt="control">
		</template>
		<template v-else>
			{{ keyObj.value }}

			<div class="key-grip" v-if="longPressWasDetected"></div>
			<div
					v-if="longPressWasDetected"
					class="key-additional-symbols"
					:style="keyObj.symbolsPopupStyles">
				<span
						v-for="symbol in keyObj.additionalSymbols"
						@mouseup="selectSymbol(symbol, keyObj)"
						class="additional-key">
					{{ symbol }}
				</span>
			</div>
		</template>

	</div>
	`,
	props: {
		keyObj: Object,
		capitalize: Boolean
	},
	data() {
		return {
			pressTimer: null,
			longPressWasDetected: false,
			isActiveNow: false
		}
	},
	created() {
		eventBus.$on('keydown', (keyCode) => {
			if (this.keyObj.keyID && this.keyObj.keyID === keyCode) {
				this.makeButtonActive();
			} else if (this.keyObj.value === keyCode) {
				this.makeButtonActive();
			}
		});
	},
	methods: {
		makeButtonActive() {
			this.isActiveNow = true;

			setTimeout(() => {
				this.isActiveNow = false;
			}, 100);
		},
		removeTimer() {
			clearTimeout(this.pressTimer);
			this.$nextTick(() => {
				this.longPressWasDetected = false;
			});
		},
		listenKeyPress(event, key) {
			this.pressTimer = window.setTimeout(() => {
				if (!key.additionalSymbols) return;

				this.longPressWasDetected = true;

			}, 300);
		},
		selectSymbol(symbol, keyObj) {
			this.$emit('keyboard-was-pressed', {
				additionalSymbols: keyObj.additionalSymbols,
				keyID: keyObj.keyID,
				value: symbol,
				symbolsWasPassed: true
			});
		},
		clickKey(key) {
			if (this.longPressWasDetected) return;
			this.$emit('keyboard-was-pressed', key);
		},
		addClass() {
			const SHIFT_KEY_CODE = 16;

			return {
				'control': this.keyObj.isControl,
				'is-active': this.keyObj.keyID === SHIFT_KEY_CODE && this.capitalize,
				'is-pressed': this.keyObj.isControl && this.isActiveNow,
				'is-scaled': !this.keyObj.isControl && this.isActiveNow
			}
		}
	}
}