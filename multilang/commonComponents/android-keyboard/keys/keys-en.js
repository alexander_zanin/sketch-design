export const chars = [
	// row 1
	{
		value: 'q',
		keyID: 81,
		caption: '1'
	},
	{
		value: 'w',
		keyID: 87,
		caption: '2'
	},
	{
		value: 'e',
		keyID: 69,
		caption: '3'
	},
	{
		value: 'r',
		keyID: 82,
		caption: '4'
	},
	{
		value: 't',
		keyID: 84,
		caption: '5'
	},
	{
		value: 'y',
		keyID: 89,
		caption: '6'
	},
	{
		value: 'u',
		keyID: 85,
		caption: '7'
	},
	{
		value: 'i',
		keyID: 73,
		caption: '8'
	},
	{
		value: 'o',
		keyID: 79,
		caption: '9'
	},
	{
		value: 'p',
		keyID: 80,
		caption: '0'
	},

	// row 2
	{
		value: 'a',
		keyID: 65
	},
	{
		value: 's',
		keyID: 83
	},
	{
		value: 'd',
		keyID: 68
	},
	{
		value: 'f',
		keyID: 70
	},
	{
		value: 'g',
		keyID: 71
	},
	{
		value: 'h',
		keyID: 72
	},
	{
		value: 'j',
		keyID: 74
	},
	{
		value: 'k',
		keyID: 75
	},
	{
		value: 'l',
		keyID: 76
	},

	// row 3
	{
		// shit key
		img: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNjNweCIgaGVpZ2h0PSI1OHB4IiB2aWV3Qm94PSIwIDAgNjMgNTgiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ5LjEgKDUxMTQ3KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5TaGlmdDwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSJhbmRyb2lkIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBvcGFjaXR5PSIwLjI0OTk5OTk4NSI+CiAgICAgICAgPGcgaWQ9IjFBLWFuZHJvaWQtQ29weSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTM2LjAwMDAwMCwgLTE2NTMuMDAwMDAwKSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2Utd2lkdGg9IjQiPgogICAgICAgICAgICA8ZyBpZD0iQW5kcm9pZC1MLUtleWJvYXJkLUxpZ2h0LU1EUEktQ29weSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4wMDAwMDAsIDEyNDguMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iU2hpZnQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDY3LjUwMDAwMCwgNDM0LjUwMDAwMCkgcm90YXRlKC05MC4wMDAwMDApIHRyYW5zbGF0ZSgtNjcuNTAwMDAwLCAtNDM0LjUwMDAwMCkgIiBwb2ludHM9IjY3LjUgNDIxLjI1IDY3LjUgNDA4IDk0IDQzNC41IDY3LjUgNDYxIDY3LjUgNDQ3Ljc1IDQxIDQ0Ny43NSA0MSA0MjEuMjUiPjwvcG9seWdvbj4KICAgICAgICAgICAgPC9nPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+',
		isControl: true,
		keyID: 16
	},
	{
		value: 'z',
		keyID: 90
	},
	{
		value: 'x',
		keyID: 88
	},
	{
		value: 'c',
		keyID: 67
	},
	{
		value: 'v',
		keyID: 86
	},
	{
		value: 'b',
		keyID: 66
	},
	{
		value: 'n',
		keyID: 78
	},
	{
		value: 'm',
		keyID: 77
	},
	{
		// delete button
		img: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNzJweCIgaGVpZ2h0PSI1NHB4IiB2aWV3Qm94PSIwIDAgNzIgNTQiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ5LjEgKDUxMTQ3KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5CYWNrc3BhY2U8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz48L2RlZnM+CiAgICA8ZyBpZD0iYW5kcm9pZCIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZmlsbC1vcGFjaXR5PSIwLjI1Ij4KICAgICAgICA8ZyBpZD0iMUEtYW5kcm9pZC1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOTc2LjAwMDAwMCwgLTE2NTguMDAwMDAwKSIgZmlsbD0iIzAwMDAwMCI+CiAgICAgICAgICAgIDxnIGlkPSJBbmRyb2lkLUwtS2V5Ym9hcmQtTGlnaHQtTURQSS1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwgMTI0OC4wMDAwMDApIj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMDQyLDQxMCBMOTk3LDQxMCBDOTk0LjksNDEwIDk5My40LDQxMC45IDk5Mi4yLDQxMi43IEw5NzYsNDM3IEw5OTIuMiw0NjEuMyBDOTkzLjQsNDYyLjggOTk0LjksNDY0IDk5Nyw0NjQgTDEwNDIsNDY0IEMxMDQ1LjMsNDY0IDEwNDgsNDYxLjMgMTA0OCw0NTggTDEwNDgsNDE2IEMxMDQ4LDQxMi43IDEwNDUuMyw0MTAgMTA0Miw0MTAgTDEwNDIsNDEwIFogTTEwMzMsNDQ3LjggTDEwMjguOCw0NTIgTDEwMTgsNDQxLjIgTDEwMDcuMiw0NTIgTDEwMDMsNDQ3LjggTDEwMTMuOCw0MzcgTDEwMDMsNDI2LjIgTDEwMDcuMiw0MjIgTDEwMTgsNDMyLjggTDEwMjguOCw0MjIgTDEwMzMsNDI2LjIgTDEwMjIuMiw0MzcgTDEwMzMsNDQ3LjggTDEwMzMsNDQ3LjggWiIgaWQ9IkJhY2tzcGFjZSI+PC9wYXRoPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=',
		isControl: true,
		keyID: 8
	},
];

export const symbols = [
	// row 1
	{
		value: '1'
	},
	{
		value: '2'
	},
	{
		value: '3'
	},
	{
		value: '4'
	},
	{
		value: '5'
	},
	{
		value: '6'
	},
	{
		value: '7'
	},
	{
		value: '8'
	},
	{
		value: '9'
	},
	{
		value: '0'
	},

	// row 2
	{
		value: '@'
	},
	{
		value: '#'
	},
	{
		value: '$'
	},
	{
		value: '_'
	},
	{
		value: '&'
	},
	{
		value: '-'
	},
	{
		value: '+'
	},
	{
		value: '('
	},
	{
		value: ')'
	},
	{
		value: '/'
	}
];

export const symbolsControls = [
	{
		alias: '=\\<',
		keyID: '=\\<',
		classes: ['fz-xs'],
		isControl: true,
	},
	{
		value: '*'
	},
	{
		value: '"'
	},
	{
		value: '\''
	},
	{
		value: ':'
	},
	{
		value: ';'
	},
	{
		value: '!'
	},
	{
		value: '?'
	},
	{
		// delete button
		img: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNzJweCIgaGVpZ2h0PSI1NHB4IiB2aWV3Qm94PSIwIDAgNzIgNTQiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ5LjEgKDUxMTQ3KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5CYWNrc3BhY2U8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz48L2RlZnM+CiAgICA8ZyBpZD0iYW5kcm9pZCIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZmlsbC1vcGFjaXR5PSIwLjI1Ij4KICAgICAgICA8ZyBpZD0iMUEtYW5kcm9pZC1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOTc2LjAwMDAwMCwgLTE2NTguMDAwMDAwKSIgZmlsbD0iIzAwMDAwMCI+CiAgICAgICAgICAgIDxnIGlkPSJBbmRyb2lkLUwtS2V5Ym9hcmQtTGlnaHQtTURQSS1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwgMTI0OC4wMDAwMDApIj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMDQyLDQxMCBMOTk3LDQxMCBDOTk0LjksNDEwIDk5My40LDQxMC45IDk5Mi4yLDQxMi43IEw5NzYsNDM3IEw5OTIuMiw0NjEuMyBDOTkzLjQsNDYyLjggOTk0LjksNDY0IDk5Nyw0NjQgTDEwNDIsNDY0IEMxMDQ1LjMsNDY0IDEwNDgsNDYxLjMgMTA0OCw0NTggTDEwNDgsNDE2IEMxMDQ4LDQxMi43IDEwNDUuMyw0MTAgMTA0Miw0MTAgTDEwNDIsNDEwIFogTTEwMzMsNDQ3LjggTDEwMjguOCw0NTIgTDEwMTgsNDQxLjIgTDEwMDcuMiw0NTIgTDEwMDMsNDQ3LjggTDEwMTMuOCw0MzcgTDEwMDMsNDI2LjIgTDEwMDcuMiw0MjIgTDEwMTgsNDMyLjggTDEwMjguOCw0MjIgTDEwMzMsNDI2LjIgTDEwMjIuMiw0MzcgTDEwMzMsNDQ3LjggTDEwMzMsNDQ3LjggWiIgaWQ9IkJhY2tzcGFjZSI+PC9wYXRoPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=',
		isControl: true,
		keyID: 8
	}
];

export const mainControls = [
	{
		alias: 'QWERTY',
		value: ' ',
		classes: ['space'],
		keyID: 32,
		isControl: true,
	},
	{
		value: '.'
	},
	{
		// enter button
		img: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNTNweCIgaGVpZ2h0PSIzM3B4IiB2aWV3Qm94PSIwIDAgNTMgMzMiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ5LjEgKDUxMTQ3KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5yZXR1cm4gaWNvbjwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSJhbmRyb2lkIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0iMUEtYW5kcm9pZC1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOTcwLjAwMDAwMCwgLTE4MTQuMDAwMDAwKSIgZmlsbD0iI0ZGRkZGRiI+CiAgICAgICAgICAgIDxnIGlkPSJBbmRyb2lkLUwtS2V5Ym9hcmQtTGlnaHQtTURQSS1Db3B5IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLjAwMDAwMCwgMTI0OC4wMDAwMDApIj4KICAgICAgICAgICAgICAgIDxnIGlkPSJHcm91cC0xMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOTQ2LjAwMDAwMCwgNTMxLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik03MC43Nzc3Nzc4LDM4LjExMTExMTEgTDc2LjIyMjIyMjIsMzguMTExMTExMSBMNzYuMjIyMjIyMiw1NC40NDQ0NDQ0IEwzNC44NDQ0NDQ0LDU0LjQ0NDQ0NDQgTDQ0LjY0NDQ0NDQsNjQuMjQ0NDQ0NCBMNDAuODMzMzMzMyw2OC4wNTU1NTU2IEwyNC41LDUxLjcyMjIyMjIgTDQwLjgzMzMzMzMsMzUuMzg4ODg4OSBMNDQuNjQ0NDQ0NCwzOS4yIEwzNC44NDQ0NDQ0LDQ5IEw3MC43Nzc3Nzc4LDQ5IEw3MC43Nzc3Nzc4LDM4LjExMTExMTEgWiIgaWQ9InJldHVybi1pY29uIj48L3BhdGg+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==',
		value: '\n',
		classes: ['enter'],
		keyID: 13,
		isControl: true,
	}
];