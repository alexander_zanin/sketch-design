import '../vue';
import './css/keyboard.css';
import AndroidKeyboardKey from './components/vue-android-keyboard-key';
import { chars, symbols, symbolsControls, mainControls } from './keys/keys';
import eventBus from '../tools/vue-event-bus';

export default {
	name: 'android-keyboard',
	components: {
		'android-keyboard-key': AndroidKeyboardKey
	},
	props: {
		lang: String
	},
	data() {
		return {
			pressedKeyCode: null,
			charsIsActive: true,
			capitalize: true,
			chars,
			symbols,
			symbolsControls,
			mainControls
		}
	},
	created() {
		this.selectSymbols();

		eventBus.$on('keydown', this.setPressedKeyCode);

		eventBus.$emit('capitalize', this.capitalize);

		eventBus.$on('outgoing-message-was-sent', this.capitalizeKeyboard);
	},
	destroyed() {
		eventBus.$off('keydown', this.setPressedKeyCode);
		eventBus.$off('outgoing-message-was-sent', this.capitalizeKeyboard);
	},
	watch: {
		pressedKeyCode() {
			if (!this.pressedKeyCode) return;
			this.checkKeyIsShift();
		},
		capitalize() {
			eventBus.$emit('capitalize', this.capitalize);
		},
		lang() {
			this.selectSymbols();
		}
	},
	methods: {
		setPressedKeyCode(keyCode) {
			this.pressedKeyCode = keyCode;
		},
		capitalizeKeyboard() {
			this.capitalize = true;
		},
		selectSymbols() {
			switch (this.lang) {
				case 'en':
					this.setKeysData(require('./keys/keys-en'));
					break;
				case 'fr':
					this.setKeysData(require('./keys/keys-fr'));
					break;
				default:
					this.setKeysData(require('./keys/keys'));
					break;
			}
		},
		setKeysData(keys) {
			for (const key in keys) {
				this[key] = keys[key];
			}
		},
		clickKey(key) {
			if (this.capitalize && key.value) {
				this.capitalize = false;

				const keyCapitalized = Object.assign({}, key);
				keyCapitalized.value = keyCapitalized.value.toUpperCase();

				eventBus.$emit('keyboard-was-pressed', keyCapitalized);
				return;
			}

			eventBus.$emit('keyboard-was-pressed', key);
		},
		findBy(arr, prop, value) {
			return arr.find(key => key[prop] === value)
		},
		switchKeyboard() {
			this.charsIsActive = !this.charsIsActive;
			eventBus.$emit('keyboard-was-switched', this.charsIsActive);
		},
		checkKeyIsShift() {
			const SHIFT_CODE = 16;

			if (this.pressedKeyCode === SHIFT_CODE && this.capitalize) {
				this.capitalize = false;
				return;
			}

			this.capitalize = this.pressedKeyCode === SHIFT_CODE;
		}
	},
	template: `
	<div class="keyboard-holder" :class="{ [lang] : lang }">
		<div class="keyboard">

			<div v-if="charsIsActive" class="chars-board" :class="{ 'capitalize' : capitalize }">
				<android-keyboard-key v-for="key in chars"
								  :keyObj="key"
								  :capitalize="capitalize"
								  :key="key.value"
								  @keyboard-was-pressed="clickKey"></android-keyboard-key>
			</div>
			<div v-else class="symbols-holder">
				<div class="symbols-board">
					<android-keyboard-key v-for="key in symbols"
									  :keyObj="key"
									  :key="key.value"
									  @keyboard-was-pressed="clickKey"></android-keyboard-key>
				</div>
				<div class="symbols-controls">
					<android-keyboard-key v-for="key in symbolsControls"
									  :class="{ 'big-control': key.isControl }"
									  :keyObj="key"
									  :key="key.value"
									  @keyboard-was-pressed="clickKey"></android-keyboard-key>
				</div>
			</div>

			<div class="main-controls">
				<div class="key switch-boards"
					 @click="switchKeyboard"
					 :class="{ 'fz-s' : charsIsActive, 'fz-xs' : !charsIsActive }">
					{{ charsIsActive ? '?123' : 'АБВ' }}
				</div>
				<div class="key mic">
					<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNDJweCIgaGVpZ2h0PSI1N3B4IiB2aWV3Qm94PSIwIDAgNDIgNTciIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ5LjEgKDUxMTQ3KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5TcGVhazwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSJhbmRyb2lkIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBvcGFjaXR5PSIwLjUiPgogICAgICAgIDxnIGlkPSIxQS1hbmRyb2lkLUNvcHkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yMDQuMDAwMDAwLCAtMTgwMC4wMDAwMDApIiBmaWxsPSIjMDAwMDAwIj4KICAgICAgICAgICAgPGcgaWQ9IkFuZHJvaWQtTC1LZXlib2FyZC1MaWdodC1NRFBJLUNvcHkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMDAwMDAwLCAxMjQ4LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPHBhdGggZD0iTTIyNSw1ODggQzIzMC4xLDU4OCAyMzQsNTg0LjEgMjM0LDU3OSBMMjM0LDU2MSBDMjM0LDU1NS45IDIzMC4xLDU1MiAyMjUsNTUyIEMyMTkuOSw1NTIgMjE2LDU1NS45IDIxNiw1NjEgTDIxNiw1NzkgQzIxNiw1ODQuMSAyMTkuOSw1ODggMjI1LDU4OCBMMjI1LDU4OCBaIE0yNDAuOSw1NzkgQzI0MC45LDU4OCAyMzMuNCw1OTQuMyAyMjUsNTk0LjMgQzIxNi42LDU5NC4zIDIwOS4xLDU4OCAyMDkuMSw1NzkgTDIwNCw1NzkgQzIwNCw1ODkuMiAyMTIuMSw1OTcuNiAyMjIsNTk5LjEgTDIyMiw2MDkgTDIyOCw2MDkgTDIyOCw1OTkuMSBDMjM3LjksNTk3LjYgMjQ2LDU4OS4yIDI0Niw1NzkgTDI0MC45LDU3OSBMMjQwLjksNTc5IFoiIGlkPSJTcGVhayI+PC9wYXRoPgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=">
				</div>
				<android-keyboard-key v-for="key in mainControls"
								  :class="key.classes"
								  :keyObj="key"
								  :key="key.value"
								  @keyboard-was-pressed="clickKey"></android-keyboard-key>
			</div>

		</div>
	</div>
	`
}