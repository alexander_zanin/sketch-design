export const currentLang = (translations) => {
	return {
		computed: {
			currentLang() {
				return translations[this.screenSettings.currentLang];
			}
		}
	}
}
