import eventBus from '../tools/vue-event-bus';

export const sendMessage = {
	data: {
		outgoingMessage: '',
		increaseFontSize: false
	},
	created() {
		eventBus.$on('symbol-was-selected', () => {
			if (this.outgoingMessage.length) {
				this.increaseFontSize = false;
				return;
			}
			this.increaseFontSize = true;
		});
	},
	methods: {
		sendMessage() {
			const newLines = /(?:\r\n|\r|\n)/g;
			this.outgoingMessage = this.outgoingMessage.replace(newLines, '<br>');

			const message = {
				isIncoming: false,
				content: this.outgoingMessage,
			};

			if (this.screenSettings.messagesWithTime) {
				message.customTime = '12:00 AM'
			}

			if (this.increaseFontSize) {
				message.increaseFontSize = this.increaseFontSize;
				this.increaseFontSize = false;
			}

			eventBus.$emit('outgoing-message-was-sent', message);
			this.outgoingMessage = '';
		}
	}
}