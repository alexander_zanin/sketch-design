import eventBus from '../tools/vue-event-bus';

export const showKeyboard = {
	methods: {
		showKeyboard(callback) {
			const keyboard = this.$refs.keyboard.$el;
			const keyboardH = keyboard.offsetHeight;
			const inputBar = this.$refs.inputBar;

			if (callback && typeof callback === 'function') callback();

			inputBar.style.marginBottom = `${keyboardH}px`;

			eventBus.$emit('keyboard-was-shown');
		},
	}
}