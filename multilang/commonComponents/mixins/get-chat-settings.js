import database from '@common/database/database';

export const getscreenSettings = {
	data() {
		return {
			loadingOverlayIsVisible: true
		}
	},
	created() {
		database.ref(this.databaseRef).once('value', (snapshot) => {
			this.loadingOverlayIsVisible = false;

			if (!snapshot.val()) return;
			this.screenSettings = snapshot.val();
		});
	}
}