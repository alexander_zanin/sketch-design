import '../../vue';
import '../css/circle.css';


export default {
	name: 'twitter-message-length',
	props: {
		outgoingMessage: String
	},
	data() {
		return {
			MAX_LENGTH: 280
		}
	},
	computed: {
		percentage() {
			const messageLength = this.outgoingMessage.length;
			if (!messageLength) return 0;

			const percentage = parseInt((messageLength * 100) / this.MAX_LENGTH);

			if (percentage >= 100) return 100;

			return percentage;
		},

		criticalLength() {
			const messageLength = this.outgoingMessage.length;
			const CHARS_LEFT = 20;
			if (messageLength >= this.MAX_LENGTH - CHARS_LEFT) {
				return this.MAX_LENGTH - messageLength;
			}
			return undefined;
		}
	},
	methods: {
		progressBarClasses() {
			return [
				`p${this.percentage}`,
				{ 'warning': this.criticalLength !== undefined },
				{ 'error': this.criticalLength <= 0}
			];
		},
	},
	template: `
	<div class="c100"
		 :class="progressBarClasses()">
		<span v-if="criticalLength !== undefined">
			{{ criticalLength }}
		</span>
		<div class="slice">
			<div class="bar"></div>
			<div class="fill"></div>
		</div>
	</div>
	`
}
