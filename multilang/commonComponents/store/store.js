import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
	keyCode: null
}

const mutations = {
	setKeyCode(state, code) {
		state.keyCode = code
	}
}

const actions = {
	setKeyCode ({ commit }, code) {
		commit('setKeyCode', code)
	}
}

export default new Vuex.Store({
	state,
	actions,
	mutations
})
