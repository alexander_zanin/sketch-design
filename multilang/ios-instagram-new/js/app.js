import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import { generateId } from '@common/helpers/generate-id';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import CommentsScreen from './components/comments-screen';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'chat-settings': screenSettings,
		'comments-screen': CommentsScreen,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-instagram-new',
		showComments: false,
		selectedPostId: null,
		selectedPost: null,
		screenSettings: {
			currentLang: 'rus',
			username: 'username',
			avatar: './img/profile-avatar.png',
			name: 'User Name',
			bio: 'bio',
			posts: [
				{
					id: generateId(),
					photo: './img/img01.jpg',
					likes: 27567,
					date: '11 НОЯБРЯ 1994 Г.',
					isLiked: false,
					comments: [
						{
							name: 'Some name',
							avatar: './img/post-avatar.png',
							text: 'Lorem ipsum.',
							time: '1ч',
							likesCount: 1,
							isLiked: false,
							comments: []
						}
					]
				}
			]
		},
	},
	filters: {
		addSpaceToLikes: (number) => {
			const numberStr = number.toString();

			let addSpace = (str, index) => {
				const strArr = str.split('');
				strArr.splice(index, 0, ' ');
				return strArr.join('');
			}

			switch (numberStr.length) {
				case 4:
					return addSpace(numberStr, 1);
				case 5:
					return addSpace(numberStr, 2);
				case 6:
					return addSpace(numberStr, 3);
				default:
					return number;
			}
		}
	},
	methods: {
		// toggleLike(post) {
		// 	post.isLiked = !post.isLiked
		// },
		toggleLike(obj) {
			obj.isLiked = !obj.isLiked;

			const likesCount = +obj.likes;

			if (obj.isLiked) {
				obj.likes = likesCount + 1;
			} else {
				obj.likes = likesCount - 1;
			}
		},

		addNewPost() {
			this.screenSettings.posts.push({
				id: generateId(),
				photo: './img/img01.jpg',
				likes: '27 567',
				date: '11 НОЯБРЯ 1994 Г.',
				isLiked: false,
				comments: []
			});
		},
		animationName() {
			if (this.showComments) {
				return 'slide';
			}
			return 'slide-reverse';
		},
		// goToComments(id) {
		// 	this.selectedPostId = id;
		// 	this.showComments = true;
		// }
		goToComments(postIndex) {
			this.selectedPostIndex = postIndex;
			this.showComments = true;
		}
	}
});