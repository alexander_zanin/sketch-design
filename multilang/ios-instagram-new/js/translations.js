export const tr = {
	rus: {
		meta: ['публикации', 'подписчики', 'подписки'],
		changeProfileBtn: 'Изменить профиль',
		likes: 'Нравится',
		comments: {
			head: 'Комментарии',
			placeholder: 'Комментировать',
			btn: 'Опубликовать',
			replyBtn: 'Ответить',
			replyMsg: 'В ответ'
		}
	},

	en: {
		meta: ['publications', 'followers', 'following'],
		changeProfileBtn: 'Edit profile',
		likes: 'Likes',
		comments: {
			head: 'Comments',
			placeholder: 'Add a comment...',
			btn: 'Post',
			replyBtn: 'Reply',
			replyMsg: 'Replying to'
		}
	},

	fr: {
		meta: ['publications', 'abonnés', 'abonnements'],
		changeProfileBtn: 'Modifier le profil',
		likes: 'Comme',
		comments: {
			head: 'Commentaires',
			placeholder: 'Ajouter un commentaire...',
			btn: 'Publier',
			replyBtn: 'Répondre',
			replyMsg: 'Réponse a'
		}
	}
}