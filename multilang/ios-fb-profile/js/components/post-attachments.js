export default {
	name: 'post-attachments',
	props: {
		post: Object
	},
	template: `
	<div class="post__attachments">
		<div class="post__attachment" v-if="post.attachment.pickedMode === 'withSingleImg'">
			<img :src="post.attachment.img" class="flexible-img" alt="">
		</div>
		<div class="post__attachment" v-if="post.attachment.pickedMode === 'withTile'">
			<ul class="post__tile post-tile">
				<li class="post-tile__item"
					v-for="(attachmentImg, attachmentImgIndex) in post.attachment.images"
					:class="{'post-tile__item--big' : attachmentImgIndex === 0 || attachmentImgIndex === 1 }">
					<img :src="attachmentImg" class="post-tile__img flexible-img" alt="">
				</li>
			</ul>
		</div>
		<div class="post__attachment" v-if="post.attachment.pickedMode === 'withVideo'">
			<figure class="post-video">
				<div class="post-video__holder">
					<img :src="post.attachment.img" class="post-video__img flexible-img" alt="">
					<img src="./img/mute.svg" alt="" class="post-video__icon">
				</div>
				<figcaption class="post-video__panel">
					<span class="post-video__button">{{ $root.currentLang.post.details }}</span>
				</figcaption>
			</figure>
		</div>
	</div>
	`
}