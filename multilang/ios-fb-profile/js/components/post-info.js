export default {
	name: 'post-info',
	props: {
		post: Object,
		withTaggedCaption: {
			type: Boolean,
			default: true
		},
		withTimeAndPlace: {
			type: Boolean,
			default: true
		}
	},
	template: `
	<div class="post__info-container">
		<div class="post__author">
			<strong class="post__author-name">
				{{ post.name }}
			</strong>
			<template 
				v-if="post.taggedUsers && withTaggedCaption" 
				v-for="(taggedUser, tagIndex) in post.taggedUsers">
				<!--{{ tagIndex === 0 ? 'c ' : 'и' }}-->
				{{ $root.currentLang.post.and }}
				<strong class="post__author-name">
					{{ taggedUser }}
				</strong>
			</template>
			<slot name="caption"></slot>
			<span class="post__geotag post-geotag" v-if="post.location">
				{{ $root.currentLang.post.geotag }}
				<strong class="post-geotag__place">
					<img src="./img/geotag-gray.svg" class="post-geotag__img" alt="">
					{{ post.location }}
				</strong>
			</span>
		</div>
		<ul class="post__date-place" v-if="withTimeAndPlace">
			<li class="post__date">
				{{ post.time }}
			</li>
			<li class="post__date">
				{{ post.place }}
				<img src="./img/globe.svg" alt="">
			</li>
		</ul>
	</div>
	`
}