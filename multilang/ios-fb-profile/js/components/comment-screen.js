import { showKeyboard } from '../../../commonComponents/mixins/show-keyboard';
import { toggleLike } from '../mixins/toggle-like';
import Post from './post';
import Comments from './commets';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';
import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';

export default {
	name: 'comments-screen',
	components: {
		'post': Post,
		'comments': Comments,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
	},
	mixins: [showKeyboard, toggleLike],
	props: {
		selectedPostId: [Number, String],
		value: Object
	},
	data() {
		return {
			settings: {},
			newCommentText: '',
			commentToReply: null,
			parentComment: null
		}
	},
	computed: {
		selectedPost() {
			return this.settings.posts.find((post) => post.id === this.selectedPostId);
		}
	},
	watch: {
		value() {
			this.settings = JSON.parse(JSON.stringify(this.value));
		}
	},
	created() {
		this.settings = JSON.parse(JSON.stringify(this.value));
	},
	mounted() {
		setTimeout(() => {
			this.$refs.commentTextarea.$el.focus();
		}, 300);
	},
	methods: {
		sendComment() {
			const commentObj = {
				name: this.settings.name,
				avatar: this.settings.avatar,
				text: this.newCommentText,
				time: 'Только что',
				likesCount: 0,
				isLiked: false,
				comments: []
			}

			if (this.parentComment) {
				this.parentComment.comments.push(commentObj);
			} else if (this.commentToReply) {
				this.pushNewComment(this.commentToReply, commentObj);
			} else {
				this.pushNewComment(this.selectedPost, commentObj);
			}

			this.cancelReplying();
			this.newCommentText = '';

			const scrollable = this.$refs.commentScreenContainer;

			this.$nextTick(() => {
				scrollable.scrollTop = scrollable.scrollHeight;
				this.$refs.commentTextarea.$el.focus();
			});

			this.$emit('input', this.settings);
		},
		pushNewComment(parentObj, newComment) {
			if (!parentObj.comments) {
				this.$set(parentObj, 'comments', []);
			}
			parentObj.comments.push(newComment);
		},
		cancelReplying() {
			this.parentComment = null;
			this.commentToReply = null;
		},
		toggleLike(obj) {
			obj.isLiked = !obj.isLiked;

			const likesCount = +obj.likesCount;

			if (obj.isLiked) {
				obj.likesCount = likesCount + 1;
				return;
			}
			obj.likesCount = likesCount - 1;
		},
		showReplyMessage({ comment, parentComment, commentDomElement }) {
			this.commentToReply = comment;
			this.parentComment = parentComment;
			this.$refs.commentTextarea.$el.focus();

			this.scrollToComment(commentDomElement);
		},
		scrollToComment(commentDomElement) {
			this.$nextTick(() => {
				const holder = this.$refs.commentScreenContainer;
				holder.scrollTop = (commentDomElement.offsetTop - holder.offsetHeight) + commentDomElement.offsetHeight;
			});
		},
	},
	template: `
	<div class="comment-screen">
		<div class="comment-screen__container" ref="commentScreenContainer">
			<post
					:post="selectedPost"
					:is-selected="true"
					:screen-settings="value"
					@like-was-clicked="toggleLike"
					@back-was-clicked="$emit('back-was-clicked')"></post>

			<comments 
						:comments="selectedPost.comments"
						@new-comment-was-added="scrollToComment"
						@like-was-clicked="toggleLike"
						@reply-was-clicked="showReplyMessage"></comments>
		
		</div>

		<div class="write-comment" ref="inputBar">
			<div v-if="commentToReply" class="write-comment__reply">
				{{ $root.currentLang.commentScreen.reply }} {{ commentToReply.name }}
				<button class="write-comment__reply-close" @click="cancelReplying">
					✕
				</button>
			</div>
			<div class="write-comment__container">
				<figure class="write-comment__camera">
					<img src="./img/camera2.svg" class="icon-camera write-comment__img" alt="">
				</figure>
				<outgoing-message
						:placeholder="$root.currentLang.commentScreen.placeholder"
						v-model="newCommentText"
						ref="commentTextarea"
						@focus.native="showKeyboard"></outgoing-message>

				<div class="write-comment__group">
					<ul class="write-comment__attachments">
						<li class="write-comment__attachment">
							<img src="./img/gif.svg" class="write-comment__img" alt="">
						</li>
						<li class="write-comment__attachment">
							<img src="./img/smile-gray.svg" class="write-comment__img" alt="">
						</li>
					</ul>
					<figure class="write-comment__send">
						<transition name="fade" mode="out-in">
							<img
									v-if="newCommentText.length"
									key="on"
									@click="sendComment"
									src="./img/send-filled.svg"
									class="write-comment__img"
									alt="">
							<img
									v-else
									key="off"
									src="./img/send.svg"
									class="write-comment__img"
									alt="">
						</transition>
					</figure>
				</div>
			</div>
			<ios-keyboard :lang="value.currentLang" ref="keyboard"></ios-keyboard>
		</div>
	</div>
	`
}