export default {
	name: 'post-meta',
	props: {
		post: Object,
		isSelected: {
			type: Boolean,
			default: false
		}
	},
	template: `
	<div class="post__meta">
		<div class="post__reactions-holder" v-if="post.likesCount">
			<ul class="post-reactions">
				<li class="post-reactions__item">
					<img src="./img/post-heart.svg" alt="" class="post-reactions__img icon-heart">
				</li>
				<li class="post-reactions__item">
					<img src="./img/post-like.svg" alt="" class="post-reactions__img">
				</li>
			</ul>
			{{ post.likesCount }}
		</div>
		<ul v-if="!isSelected" class="post__meta-items">
			<li class="post__meta-item" v-if="post.commentsCaption">
				{{ post.commentsCaption }}
			</li>
			<li class="post__meta-item" v-if="post.reposts">
				{{ post.reposts }}
			</li>
		</ul>
	</div>
	`
}