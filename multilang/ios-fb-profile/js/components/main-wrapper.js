export default {
	name: 'main-wrapper',
	props: {
		value: Number
	},
	mounted() {
		this.$refs.main.scrollTop = this.value;
	},
	beforeDestroy() {
		this.$emit('input', this.$refs.main.scrollTop);
	},
	template: `
	<div ref="main" class="main">
		<slot></slot>
	</div>
	`
}