import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';

export default {
	name: 'privacy-popup',
	components: {
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard
	},
	props: {
		value: Object
	},
	data() {
		return {
			pickedMode: '',
			inputs: [
				{
					value: 'public',
					title: this.$root.currentLang.privacyPopup.inputs[0].title,
					caption: this.$root.currentLang.privacyPopup.inputs[0].caption
				},
				{
					value: 'friends',
					title: this.$root.currentLang.privacyPopup.inputs[1].title,
					caption: this.$root.currentLang.privacyPopup.inputs[1].caption
				},
				{
					value: 'onlyMe',
					title: this.$root.currentLang.privacyPopup.inputs[2].title,
					caption: this.$root.currentLang.privacyPopup.inputs[2].caption
				}
			]
		}
	},
	created() {
		this.pickedMode = this.value.value;
	},
	methods: {
		selectSetting() {
			const selectedSetting = this.inputs.find((input) => {
				return input.value === this.pickedMode;
			});

			this.$emit('input', selectedSetting);
			this.$emit('hide-was-clicked');
		}
	},
	template: `
		<div class="privacy-popup with-grip">
			<div class="privacy-popup__head">
				<span class="privacy-popup__button" @click="$emit('hide-was-clicked')">
					{{ $root.currentLang.privacyPopup.head[0] }}
				</span>
				<span class="privacy-popup__title">{{ $root.currentLang.privacyPopup.head[1] }}</span>
				<span class="privacy-popup__button" @click="selectSetting">
					{{ $root.currentLang.privacyPopup.head[2] }}
				</span>
			</div>
			<div class="privacy-popup__main">
				<div class="privacy-popup__caption">
					{{ $root.currentLang.privacyPopup.caption }}
				</div>
				<ul class="privacy-popup-list">
					<li class="privacy-popup-list__item" v-for="input in inputs">
						<label class="facebook-rounded-radiobutton">
							<input 
									type="radio"
									v-model="pickedMode"
									:value="input.value" 
									class="facebook-rounded-radiobutton__input">
							<div class="facebook-rounded-radiobutton__holder">
								<div class="facebook-rounded-radiobutton__inner">
									<div class="facebook-rounded-radiobutton__title">
										{{ input.title }}
									</div>
									<div class="facebook-rounded-radiobutton__caption">
										{{ input.caption }}
									</div>
								</div>
							</div>
						</label>
					</li>
				</ul>
				<div class="privacy-popup__more">
					{{ $root.currentLang.privacyPopup.more }}
				</div>
			</div>
		</div>
	`,
}