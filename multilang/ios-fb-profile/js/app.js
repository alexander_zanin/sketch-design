import '../css/index.scss';
import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import { currentLang } from '@common/mixins/current-lang';
import { generateId } from '@common/helpers/generate-id';
import { toggleLike } from './mixins/toggle-like';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import { posts } from './posts';
import CommentsScreen from './components/comment-screen';
import NewStatusScreen from './components/new-status';
import NewPostScreen from './components/new-post';
import MainWrapper from './components/main-wrapper';
import RepostPopup from './components/repost-popup';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { tr } from './translations';
import Post from './components/post';

new Vue({
	el: '#app',
	components: {
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'new-status-screen': NewStatusScreen,
		'new-post-screen': NewPostScreen,
		'comments-screen': CommentsScreen,
		'post': Post,
		'repost-popup': RepostPopup,
		'loading-overlay': LoadingOverlay,
		'main-wrapper': MainWrapper,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, toggleLike, currentLang(tr)],
	data: {
		databaseRef: 'ios-fb-profile',
		animationName: '',

		scrollTop: 0,
		postForShare: {},
		selectedPostId: null,
		showPost: false,
		createNewStatus: false,
		createNewPostPopup: false,
		showSharePopup: false,
		screenSettings: {
			currentLang: 'rus',
			avatar: './img/profile-avatar.jpg',
			background: './img/profile-bg.jpg',
			name: 'UserName',
			status: 'Status',
			university: 'Университете',
			place: 'Москва',
			hometown: 'Calgary',
			newPostAttachment: './img/attachment.jpg',
			profilePhotos: [
				'./img/tile-img-big.jpg',
				'./img/tile-img-big.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg'
			],
			friendsCount: 251,
			profileFriends: [
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: true,
					updateText: '6 new posts'
				},
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: true,
					updateText: '6 new posts'
				},
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: true,
					updateText: '6 new posts'
				},
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: false,
					updateText: '6 new posts'
				},
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: false,
					updateText: '6 new posts'
				},
				{
					avatar: './img/friend-avatar.jpg',
					name: 'Username',
					withUpdates: false,
					updateText: '6 new posts'
				}
			],
			posts,
		}
	},
	// created() {
	// 	['avatar', 'name', 'place'].forEach((item) => {
	// 		this.$watch(`screenSettings.${item}`, () => {
	// 			this.screenSettings.posts.forEach((post, i, arr) => {
	// 				arr[i][item] = this.screenSettings[item]
	// 			});
	// 		});
	// 	});
	// },
	beforeDestroy() {
		console.log('aaaaa');
		console.log(this.$refs.main.scrollTop);
	},
	methods: {
		defaultPost() {
			return {
				id: generateId(),
				isLiked: false,
				avatar: this.screenSettings.avatar,
				name: this.screenSettings.name,
				location: 'Placeместо',
				taggedUsers: [],
				time: '2 марта в 19:33',
				place: this.screenSettings.place,
				text: 'Lorem ipsum dolor sit amet, consectetur',
				attachment: {
					exists: true,
					pickedMode: 'withSingleImg',
					img: './img/img01.jpg',
					images: [
						'./img/tile-img-big.jpg',
						'./img/tile-img-big.jpg',
						'./img/tile-img-small.jpg',
						'./img/tile-img-small.jpg',
						'./img/tile-img-small.jpg'
					]
				},
				likesCount: 91,
				commentsCaption: '1 комментарий',
				reposts: 'Перепосты: 1',
				comments: []
			}
		},
		goToComments(id) {
			this.goToScreen('slide', 'showPost');
			this.selectedPostId = id;
		},
		backToFeed(animationName, prop) {
			this.animationName = animationName;
			this[prop] = false;
		},
		goToScreen(animationName, prop) {
			this.animationName = animationName;
			this[prop] = true;
		},
		createNewPost(newPostText, withAttachment) {
			this.screenSettings.posts.unshift({
				id: generateId(),
				isLiked: false,
				avatar: this.screenSettings.avatar,
				name: this.screenSettings.name,
				location: '',
				taggedUsers: [],
				time: 'Только что',
				place: '',
				text: newPostText,
				attachment: {
					exists: withAttachment,
					pickedMode: 'withSingleImg',
					img: this.screenSettings.newPostAttachment,
					images: []
				},
				likesCount: 0,
				commentsCaption: '',
				reposts: '',
				comments: []
			});

			this.screenSettings.newPostAttachment = './img/attachment.jpg';
			this.backToFeed('slide-bottom', withAttachment ? 'createNewStatus' : 'createNewPostPopup');
		},
		showSharePost(post) {
			this.showSharePopup = true;
			this.postForShare = post;
		},
		closeSharePopup() {
			this.showSharePopup = false;
			this.postForShare = {};
		},
		sharePost(shareText) {
			const post = Object.assign({}, this.postForShare);
			post.id = generateId();
			post.isShared = true;
			post.taggedUsers = [this.screenSettings.name];
			post.sharedPerson = {
				avatar: this.screenSettings.avatar,
				time: 'Только что',
				name: this.screenSettings.name,
				text: shareText,
				isLiked: false,
				likesCount: 0
			};
			post.caption = this.currentLang.post.caption;
			this.screenSettings.posts.unshift(post);

			this.postForShare = {};
		}
	}
});