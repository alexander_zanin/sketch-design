import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import MessageLength from '@common/twitter/js/twitter-message-length';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'message-length' : MessageLength,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-new-tweet',
		tweetMessage: '',
		showNewTweet: false,
		tweets: [],
		screenSettings: {
			currentLang: 'rus',
			avatar: './img/avatar.png',
			name: 'User',
			username: 'Stokely Carmichael',
			placeholder: 'What\'s happening',
			posts: [
				{
					name: 'User',
					username: 'NewYorkTimes',
					avatar: './img/otherpostcomment copy bitmap.png',
					text: 'We’ll deliver the best stories and ideas on the topics you care about most straight to your homepage',
					time: '13м',
					img: './img/img.png',
					key: Math.random()
				},
				{
					name: 'User',
					username: 'NewYorkTimes',
					avatar: './img/otherpostcomment copy bitmap.png',
					text: 'Негр убит во время шествия в Мемфисе',
					time: '13м',
					img: './img/img.png',
					key: Math.random()
				},
				{
					name: 'User',
					username: 'NewYorkTimes',
					avatar: './img/otherpostcomment copy bitmap.png',
					text: 'Негр убит во время шествия в Мемфисе',
					time: '13м',
					img: './img/img.png',
					key: Math.random()
				},
				{
					name: 'User',
					username: 'NewYorkTimes',
					avatar: './img/otherpostcomment copy bitmap.png',
					text: 'Негр убит во время шествия в Мемфисе',
					time: '13м',
					img: './img/img.png',
					key: Math.random()
				},
				{
					name: 'User',
					username: 'NewYorkTimes',
					avatar: './img/otherpostcomment copy bitmap.png',
					text: 'Негр убит во время шествия в Мемфисе',
					time: '13м',
					img: './img/img.png',
					key: Math.random()
				},
			]
		}
	},
	methods: {
		showPopup() {
			this.showNewTweet = true;

			const ANIMATON_DURATON = 250;

			setTimeout(() => {
				this.$refs.textarea.$el.focus();
			}, ANIMATON_DURATON);
		},
		hidePopup() {
			this.showNewTweet = false;
		},
		sendMessage() {
			const newLines = /(?:\r\n|\r|\n)/g;
			this.tweetMessage = this.tweetMessage.replace(newLines, '<br>');

			const message = {
				name: this.screenSettings.name,
				username: this.screenSettings.username,
				avatar: this.screenSettings.avatar,
				text: this.tweetMessage,
				time: '1c',
				key: Math.random()
				// content: this.tweetMessage,
				// key: Math.random() // for list animation
			};
			// this.tweets.unshift(message);
			this.screenSettings.posts.unshift(message);
			this.tweetMessage = '';
		}
	}
});