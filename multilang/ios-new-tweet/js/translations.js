export const tr = {
	rus: {
		head: 'Главная',
		btn: 'Твитнуть',
		placeholder: 'Что нового?'
	},

	en: {
		head: 'Home',
		btn: 'Tweet',
		placeholder: 'What\'s happening?'
	},

	fr: {
		head: 'Accueil\n',
		btn: 'Tweeter',
		placeholder: 'Quoi de neuf ?'
	}
}