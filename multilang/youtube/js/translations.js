export const tr = {
	rus: {
		share: 'Поделиться',
		addTo: 'Добавить в',
		subscribe: 'ПОДПИСАТЬСЯ',
		comments: 'Комментарии',
		showReply: ['ПОКАЗАТЬ ОТВЕТ', 'ПОКАЗАТЬ'],
		commentTime: 'только что',
		placeholder: 'Оставьте комментарий...'
	},

	en: {
		share: 'Поделиться',
		addTo: 'Share',
		subscribe: 'SUBSCRIBE',
		comments: 'Comments',
		showReply: ['VIEW REPLY', 'VIEW'],
		commentTime: 'just now',
		placeholder: 'Add a public comment...'
	},

	fr: {
		share: 'Patager',
		addTo: 'Ajouter à',
		subscribe: 'S\'ABONNER',
		comments: 'Commentaires',
		showReply: ['AFFICHER LA RÉPONSE', 'AFFICHER LA'],
		commentTime: 'juste maintenant',
		placeholder: 'Ajeouter un commentaire public...'
	}
}