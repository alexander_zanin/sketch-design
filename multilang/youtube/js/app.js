import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import MessageLength from '@common/twitter/js/twitter-message-length';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';


new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'message-length': MessageLength,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-youtube',
		incomingComment: {
			name: '',
			text: ''
		},
		screenSettings: {
			currentLang: 'rus',
			date: '12 марта 2018г.',
			views: 'Нет просмотров',
			channelName: 'Username',
			channelAvatar: './img/avatar.png',
			subscribers: '14 подписчиков',
			commentsCount: '3 тыс.',
			authorAvatar: './img/avatar.png',
			video: {
				likes: {
					count: 0,
					isActive: false
				},
				dislikes: {
					count: 0,
					isActive: false
				},
			},
			comments: [
				{
					name: 'mypage',
					content: 'Весьма увлекательно',
					likes: {
						count: 3,
						isActive: true
					},
					dislikes: {
						count: 0,
						isActive: false
					},
					comments: 0,
					time: '5 дней назад'
				},
				{
					name: 'username',
					content: 'Не впечатлен',
					likes: {
						count: 0,
						isActive: false
					},
					dislikes: {
						count: 0,
						isActive: false
					},
					comments: 1,
					time: 'только что'
				},
				{
					name: 'davediv',
					content: 'Где скачать?',
					likes: {
						count: 0,
						isActive: false
					},
					dislikes: {
						count: 0,
						isActive: false
					},
					comments: 0,
					time: '3 дня назад'
				},
				{
					name: 'davediv',
					content: 'смутно похоже на правду',
					likes: {
						count: 0,
						isActive: false
					},
					dislikes: {
						count: 3,
						isActive: true
					},
					comments: 0,
					time: '3 дня назад'
				}
			]
		},
		outgoingComment: '',
		showNewTweet: false
	},
	methods: {
		showKeyboard() {
			this.keyboardIsVisible = true;
			this.toggleKeyboard();
		},
		hideKeyboard() {
			this.keyboardIsVisible = false;
			this.toggleKeyboard();
		},
		toggleKeyboard() {
			const keyboard = this.$refs.keyboardWrapper;

			if (this.keyboardIsVisible) {
				const ANIMATON_DURATON = 300;

				keyboard.setAttribute('style', `transform: translateY(0)`);

				setTimeout(() => {
						this.$refs.textarea.$el.focus();
				}, ANIMATON_DURATON);
			} else {
				keyboard.removeAttribute('style');
			}
		},
		reactToPost(e, post, reactionObjName) {
			this.reactionToggleClass(e);
			const opositeReactionObjName = reactionObjName === 'likes' ? 'dislikes' : 'likes';
			const currentReactionObj = post[reactionObjName];
			const opositeReactionObj = post[opositeReactionObjName];

			currentReactionObj.count = parseInt(currentReactionObj.count);
			if (currentReactionObj.isActive) {
				currentReactionObj.isActive = false;
				currentReactionObj.count -= 1;
				return;
			}

			if (opositeReactionObj.isActive) {
				opositeReactionObj.count -= 1;
				opositeReactionObj.isActive = false;
			}

			currentReactionObj.isActive = true;
			currentReactionObj.count += 1;
		},
		reactionToggleClass(e) {
			const currentTarget = e.currentTarget;

			currentTarget.classList.add('is-active');

			setTimeout(() => {
				currentTarget.classList.remove('is-active');
			}, 200);
		},
		sendMessage() {
			const newLines = /(?:\r\n|\r|\n)/g;
			this.outgoingComment = this.outgoingComment.replace(newLines, '<br>');

			const message = {
				name: 'somename',
				content: this.outgoingComment,
				likes: {
					count: 0,
					isActive: false
				},
				dislikes: {
					count: 0,
					isActive: false
				},
				comments: 0,
				time: this.currentLang.commentTime
			};
			this.screenSettings.comments.unshift(message);
			this.outgoingComment = '';

			this.hideKeyboard();
		},
		sendIncomingComment() {
			const newLines = /(?:\r\n|\r|\n)/g;
			this.incomingComment.text = this.incomingComment.text.replace(newLines, '<br>');

			const message = {
				name: this.incomingComment.name,
				content: this.incomingComment.text,
				likes: {
					count: 0,
					isActive: false
				},
				dislikes: {
					count: 0,
					isActive: false
				},
				comments: 0,
				time: this.currentLang.commentTime
			};
			this.screenSettings.comments.unshift(message);
			this.incomingComment.name = '';
			this.incomingComment.text = '';
		},
		replyEndingRus(number) {
			if (number / 10 >= 1 && number / 10 < 2) {
				return 'ОТВЕТОВ'
			}
			if (number % 10 === 1) {
				return 'ОТВЕТ';
			}
			const lastN = number.toString().split('').pop();

			const range = '234';

			if (range.includes(lastN)) {
				return 'ОТВЕТА';
			} else  {
				return 'ОТВЕТОВ';
			}
		},
		replyEnding(number) {
			switch (this.screenSettings.currentLang) {
				case 'rus':
					return this.replyEndingRus(number);
				case 'en':
					return 'REPLIES';
				case 'fr':
					return 'RÉPONSES';
			}
		}
	}
});