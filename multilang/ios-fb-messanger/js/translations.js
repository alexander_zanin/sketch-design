export const tr = {
	rus: {
		back: 'Главная',
		status: 'Онлайн'
	},

	en: {
		back: 'Home',
		status: 'Active now'
	},

	fr: {
		back: 'Accueil',
		status: 'Actif'
	}
}