import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { showKeyboard } from '@common/mixins/show-keyboard';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';



new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, showKeyboard, getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-fb-messanger',
		textareaWasClicked: false,
		interlocutor: {
			isTyping: false,
			isOnline: false
		},
		screenSettings: {
			currentLang: 'rus',
			avatar: './img/1a iosavatar.png',
			name: 'Michael Scott',
			placeholder: 'Aa'
		}
	},
	methods: {
		showKeyboardCallback() {
			this.textareaWasClicked = true;
		},
		checkTypingStatus(interlocutorIsTyping) {
			this.interlocutor.isTyping = interlocutorIsTyping;
		},
		checkOnlineStatus(interlocutorIsOnline) {
			this.interlocutor.isOnline = interlocutorIsOnline
		}
	}
});