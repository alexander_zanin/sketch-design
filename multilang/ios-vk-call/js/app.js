import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';

new Vue({
	el: '#app',
	components: {
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay
	},
	mixins: [getscreenSettings],
	data: {
		databaseRef: 'ios-vk-call',
		intervalId: null,
		screenSettings: {
			avatar: './img/avatar.png',
			name: 'Михаил Лихачев',
			placeholder: '',
			callMinutes: 0,
			callSeconds: 28
		}
	},
	methods: {
		getNumber(number) {
			const n = parseInt(number).toString();

			if (n.length > 1) return n;

			return `0${n}`
		},
		startCallTimer() {
			this.intervalId = setInterval(() => {
				this.screenSettings.callSeconds = parseInt(this.screenSettings.callSeconds);
				this.screenSettings.callMinutes = parseInt(this.screenSettings.callMinutes);

				if (this.screenSettings.callSeconds === 59) {
					this.screenSettings.callMinutes += 1;
					this.screenSettings.callSeconds = 0;
					return;
				}

				this.screenSettings.callSeconds += 1;
			}, 1000);
		},
		endCallTimer() {
			clearInterval(this.intervalId);
		}
	}
});