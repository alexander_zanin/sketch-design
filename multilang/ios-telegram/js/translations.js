export const tr = {
	rus: {
		back: 'Чаты',
		typing: 'печатает',
		status: 'Онлайн',
	},

	en: {
		back: 'Chats',
		typing: 'typing',
		status: 'Online',
	},

	fr: {
		back: 'Chats',
		typing: 'dactylographié',
		status: 'En ligne',
	}
}