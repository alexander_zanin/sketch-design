export const tr = {
	rus: {
		head: 'Заметки',
		toolsCaption: 'Заметка…',
		newItemCaption: 'Новый пункт'
	},

	en: {
		head: 'Notes',
		toolsCaption: 'Note…',
		newItemCaption: 'New item'
	},

	fr: {
		head: 'Remarques',
		toolsCaption: 'Remarque…',
		newItemCaption: 'Nouvel article'
	}
}