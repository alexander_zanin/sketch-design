import Vue from '@common/vue';
import NewNote from '@common/messaging/vue-outgoing-message';
import AndroidKeyboard from '@common/android-keyboard/vue-android-keyboard';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'new-note': NewNote,
		'android-keyboard': AndroidKeyboard,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [currentLang(tr)],
	data: {
		note: '',
		showNewNoteScreen: false,
		databaseRef: 'android-notes',
		screenSettings: {
			currentLang: 'rus'
		}
	},
	methods: {
		showNewNote() {
			this.showNewNoteScreen = true;

			if (this.showNewNoteScreen) {
				const ANIMATION_DURATION = 300;

				setTimeout(() => {
					this.$refs.textarea.$el.focus();
				}, ANIMATION_DURATION);
			}
		}
	}
});