import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import { currentLang } from '@common/mixins/current-lang';
import { generateId } from '@common/helpers/generate-id';
import { toggleLike } from './mixins/toggle-like';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import MessagesList from '@common/messaging/vue-messages-list';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import CommentsScreen from './components/comment-screen';
import NewStatusScreen from './components/new-status';
import NewPostScreen from './components/new-post';
import RepostPopup from './components/repost-popup';
import Post from './components/post';
import CheckIn from './components/chek-in';
import { posts } from './posts';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'messages-list' : MessagesList,
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'new-status-screen': NewStatusScreen,
		'new-post-screen': NewPostScreen,
		'comments-screen': CommentsScreen,
		'post': Post,
		'check-in': CheckIn,
		'repost-popup': RepostPopup,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, toggleLike, currentLang(tr)],
	data: {
		databaseRef: 'ios-fb-feed',
		animationName: '',

		postForShare: {},
		selectedPostId: null,
		showPost: false,
		createNewStatus: false,
		createNewPostPopup: false,
		createCheckIn: false,
		showSharePopup: false,
		screenSettings: {
			currentLang: 'rus',
			avatar: './img/avatar.png',
			name: 'Your Name',
			newPostAttachment: './img/attachment.jpg',
			newCheckInAttachment: './img/attachment.jpg',
			posts,
		}
	},
	methods: {
		defaultPost() {
			return {
				id: generateId(),
				isLiked: false,
				avatar: './img/avatar.png',
				name: 'UserName',
				location: 'Placeместо',
				taggedUsers: [],
				time: '2 марта в 19:33',
				place: 'Москва',
				text: 'Lorem ipsum dolor sit amet, consectetur',
				attachment: {
					exists: true,
					pickedMode: 'withSingleImg',
					img: './img/img01.jpg',
					images: [
						'./img/tile-img-big.jpg',
						'./img/tile-img-big.jpg',
						'./img/tile-img-small.jpg',
						'./img/tile-img-small.jpg',
						'./img/tile-img-small.jpg'
					]
				},
				likesCount: 91,
				commentsCaption: '1 комментарий',
				reposts: 'Перепосты: 1',
				comments: []
			}
		},
		goToComments(id) {
			this.goToScreen('slide', 'showPost');
			this.selectedPostId = id;
		},
		backToFeed(animationName, prop) {
			this.animationName = animationName;
			this[prop] = false;
		},
		goToScreen(animationName, prop) {
			this.animationName = animationName;
			this[prop] = true;
		},
		createNewPost(newPostText, withAttachment, type) {
			const post = {
				id: generateId(),
				isLiked: false,
				avatar: this.screenSettings.avatar,
				name: this.screenSettings.name,
				location: '',
				taggedUsers: [],
				time: 'Только что',
				place: '',
				text: newPostText,
				attachment: {
					exists: withAttachment,
					pickedMode: 'withSingleImg',
					img: type === 'check-in' ? this.screenSettings.newCheckInAttachment : this.screenSettings.newPostAttachment,
					images: []
				},
				likesCount: 0,
				commentsCaption: '',
				reposts: '',
				comments: []
			};

			if (type === 'check-in') {
				post.caption = this.currentLang.post.checkedIn
				post.checkedInPlace = 'Place'
				post.checkedInPlaceType = 'Type'
				post.checkedInPlaceAvatar = './img/avatar.png'
				post.isCheckIn = true
			}

			this.screenSettings.posts.unshift(post);

			this.screenSettings.newPostAttachment = './img/attachment.jpg';
			this.backToFeed('slide-bottom', withAttachment ? 'createNewStatus' : 'createNewPostPopup');
		},
		showSharePost(post) {
			this.showSharePopup = true;
			this.postForShare = post;
		},
		closeSharePopup() {
			this.showSharePopup = false;
			this.postForShare = {};
		},
		sharePost(shareText) {
			const post = Object.assign({}, this.postForShare);
			post.id = generateId();
			post.isShared = true;
			post.taggedUsers = [];
			post.sharedPerson = {
				avatar: this.screenSettings.avatar,
				time: 'Только что',
				name: this.screenSettings.name,
				text: shareText,
				isLiked: false,
				likesCount: 0
			};
			post.caption = this.currentLang.post.caption;
			this.screenSettings.posts.unshift(post);

			this.postForShare = {};
		}
	}
});