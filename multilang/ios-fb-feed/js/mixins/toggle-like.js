export const toggleLike = {
	methods: {
		toggleLike(obj) {
			obj.isLiked = !obj.isLiked;

			const likesCount = +obj.likesCount;

			if (obj.isLiked) {
				obj.likesCount = likesCount + 1;
				return;
			}
			obj.likesCount = likesCount - 1;
		}
	}
}