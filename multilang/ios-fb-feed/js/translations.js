export const tr = {
	rus: {
		feed: {
			search: 'Поиск',
			addNewPost: {
				top: 'О чем вы думаете?',
				tool1: 'Эфир',
				tool2: 'Фото',
				tool3: 'Отметить<br>посещение'
			},
			stories: 'Истории'
		},

		post: {
			and: 'и',
			geotag: 'находится в',
			details: 'Подробнее',
			caption: 'поделился публикацией',
			tools: ['Нравится', 'Комментарий', 'Поделиться'],
			checkedIn: 'отметился в',
			checkInBtn: 'Обзор'
		},

		comment: {
			like: 'Нравится',
			reply: 'Ответить'
		},

		newPost: {
			header: [
				'Отмена',
				'Создать пуб...',
				'Поделиться'
			],
			placeholder: 'О чем вы думаете?',
			tools: ['Доступно всем', 'Альбом'],
			additional: 'Дополните публикацию',
			attachments: [
				'Фото/видео',
				'Чувства/действия/наклейки',
				'Отметить посещение',
				'Прямой эфир',
				'Камера',
				'Продажа'
			]
		},

		newCheckIn: {
			header: [
				'Отмена',
				'Отметиться',
				'Поделиться'
			],
			placeholder: 'Скажите что-то об этом месте',
			tools: ['Доступно всем', 'Альбом'],
			additional: 'Дополните публикацию',
			attachments: [
				'Фото/видео',
				'Чувства/действия/наклейки',
				'Отметить посещение',
				'Прямой эфир',
				'Камера',
				'Продажа'
			]
		},

		newStatus: {
			header: [
				'Отмена',
				'Обновить статус',
				'Поделиться'
			],
			placeholder: 'Скажите что-нибудь об этом фото...',
		},

		commentScreen: {
			reply: 'В ответ',
			placeholder: 'Комментарий...'
		},

		repostPopup: {
			postTool: 'Лента новостей',
			shareButton: 'Поделиться',
			shareLinks: ['Отправить в Messenger', 'Скопировать ссылку'],
			placeholder: 'Скажите что-нибудь об этом...'
		},

		privacyPopup: {
			head: ['Отмена', 'Установите настройки...', 'Готово'],
			caption: 'Кто может видеть вашу публикацию?',
			inputs: [
				{
					title: 'Доступно всем',
					caption: 'Все на Facebook и вне его',
				},
				{
					title: 'Друзья',
					caption: 'Ваши друзья на Facebook',
				},
				{
					title: 'Только я',
					caption: 'Только я',
				}
			],
			more: 'Еще'
		}
	},

	en: {
		feed: {
			search: 'Search',
			addNewPost: {
				top: 'What\'s on your mind?',
				tool1: 'Live',
				tool2: 'Photo',
				tool3: 'Check In'
			},
			stories: 'Stories'
		},

		post: {
			and: 'and',
			geotag: 'at',
			details: 'Details',
			caption: 'shared the publication',
			tools: ['Like', 'Comment', 'Share'],
			checkedIn: 'checked in',
			checkInBtn: 'Review'
		},

		comment: {
			like: 'Like',
			reply: 'Reply'
		},

		newPost: {
			header: [
				'Cancel',
				'Create post',
				'Share'
			],
			placeholder: 'What\'s on your mind',
			tools: ['Public', 'Album'],
			additional: 'Add to your post',
			attachments: [
				'Photo/Video',
				'Feelings/Activity/Sticker',
				'Check In',
				'Live Video',
				'Camera',
				'Sell Something'
			]
		},

		newStatus: {
			header: [
				'Cancel',
				'Update Status',
				'Share'
			],
			placeholder: 'Say something about this photo...',
		},

		commentScreen: {
			reply: 'В ответ',
			placeholder: 'Write a comment…'
		},

		repostPopup: {
			postTool: 'News Feed',
			shareButton: 'Share Now',
			shareLinks: ['Send in Messenger', 'Copy Link'],
			placeholder: 'Say something about this...'
		},

		privacyPopup: {
			head: ['Cancel', 'Select Privacy', 'Done'],
			caption: 'Who can see your post?',
			inputs: [
				{
					title: 'Public',
					caption: 'Anyone on or off Facebook',
				},
				{
					title: 'Friends',
					caption: 'Your friends on Facebook',
				},
				{
					title: 'Only me',
					caption: 'Only me',
				}
			],
			more: 'More'
		}
	},

	fr: {
		feed: {
			search: 'Rechercher',
			addNewPost: {
				top: 'Exprimez-vous',
				tool1: 'En direct',
				tool2: 'Photo',
				tool3: 'Je suis là'
			},
			stories: 'Stories'
		},

		post: {
			and: 'et',
			geotag: 'est en',
			details: 'Détails',
			caption: 'a partagé la publication',
			tools: ['J\'aime', 'Commenter', 'Partager'],
			checkedIn: 'enregistré',
			checkInBtn: 'La revue\n'
		},

		comment: {
			like: 'J\'aime',
			reply: 'Répondre'
		},

		newPost: {
			header: [
				'Annuler',
				'Créer une publication',
				'Partager'
			],
			placeholder: 'Exprimez-vous',
			tools: ['Fil d\'actualité', 'Album'],
			additional: 'Complétez la publication',
			attachments: [
				'Photo/Vidéo',
				'Humeur/Activité/Sticker',
				'Je suis là',
				'Vidéo in direct',
				'Caméra',
				'À vendre'
			]
		},

		newStatus: {
			header: [
				'Annuler',
				'Mise à jour',
				'Partager'
			],
			placeholder: 'Dites quelque chose à propos de cette photo...',
		},

		commentScreen: {
			reply: 'Répondre à',
			placeholder: 'Votre commentaire...'
		},

		repostPopup: {
			postTool: 'Fil d\'actualité',
			shareButton: 'Partager maintenant',
			shareLinks: ['Отправить в Messenger', 'Скопировать ссылку'],
			placeholder: 'Dites quelque chose à propos de cecci...'
		},

		privacyPopup: {
			head: ['Annuler', 'Sélectionner la confidentialité', 'Fin'],
			caption: 'Qui peut voir votre publication ?',
			inputs: [
				{
					title: 'Public',
					caption: 'Tout le monde sur ou en dehours de Facebook',
				},
				{
					title: 'Amis',
					caption: 'Vos amis sur Facebook',
				},
				{
					title: 'Moi uniquement',
					caption: 'Moi uniquement',
				}
			],
			more: 'Afficher plus...'
		}
	}
}