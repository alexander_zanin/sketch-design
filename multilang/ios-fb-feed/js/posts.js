export const posts = [
	{
		id: Math.random(),
		isLiked: false,
		avatar: './img/avatar.png',
		name: 'UserName',
		location: 'Placeместо',
		taggedUsers: [],
		time: '2 марта в 19:33',
		place: 'Москва',
		text: 'Lorem ipsum dolor sit amet, consectetur',
		attachment: {
			exists: true,
			pickedMode: 'withSingleImg',
			img: './img/img01.jpg',
			images: [
				'./img/tile-img-big.jpg',
				'./img/tile-img-big.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg'
			]
		},
		likesCount: 91,
		commentsCaption: '1 комментарий',
		reposts: 'Перепосты: 1',
		comments: [
			{
				name: 'Some Name',
				avatar: './img/avatar.png',
				text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
				time: '1h',
				likesCount: 0,
				isLiked: false,
				comments: []
			}
		]
	},
	{
		id: Math.random(),
		isLiked: false,
		avatar: './img/avatar.png',
		name: 'UserName',
		location: '',
		taggedUsers: [],
		time: '2 марта в 19:33',
		place: 'Москва',
		text: 'Lorem ipsum dolor sit amet, consectetur',
		attachment: {
			exists: true,
			pickedMode: 'withTile',
			img: './img/img01.jpg',
			images: [
				'./img/tile-img-big.jpg',
				'./img/tile-img-big.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg'
			]
		},
		likesCount: 91,
		commentsCaption: '1 комментарий',
		reposts: 'Перепосты: 1',
		comments: []
	},
	{
		id: Math.random(),
		isLiked: false,
		avatar: './img/avatar.png',
		name: 'UserName',
		location: '',
		taggedUsers: [],
		time: '2 марта в 19:33',
		place: 'Москва',
		text: 'Lorem ipsum dolor sit amet, consectetur',
		attachment: {
			exists: true,
			pickedMode: 'withVideo',
			img: './img/img01.jpg',
			views: '1K',
			images: [
				'./img/tile-img-big.jpg',
				'./img/tile-img-big.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg',
				'./img/tile-img-small.jpg'
			]
		},
		likesCount: 91,
		commentsCaption: '1 комментарий',
		reposts: 'Перепосты: 1',
		comments: []
	}
];