import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';
import ClickOutside from 'vue-click-outside';
import PrivacyPopup from './privacy-popup';

export default {
	name: 'repost-popup',
	components: {
		'outgoing-message': OutgoingMessage,
		'privacy-popup': PrivacyPopup,
		'ios-keyboard': IosKeyboard
	},
	directives: {
		ClickOutside
	},
	props: {
		screenSettings: Object,
	},
	data() {
		return {
			shareText: '',
			textareaWasFocused: false,
			privacyPopupIsVisible: false,
			privacySetting: {
				value: 'friends',
				title: this.$root.currentLang.privacyPopup.inputs[1].title,
				caption: this.$root.currentLang.privacyPopup.inputs[1].caption
			},
			popupsAnimationName: 'slide'
		}
	},
	watch: {
		privacyPopupIsVisible() {
			if (this.privacyPopupIsVisible) {
				this.popupsAnimationName = 'slide';
				return
			}
			this.popupsAnimationName = 'slide-reverse';
		}
	},
	methods: {
		showKeyboard() {
			this.textareaWasFocused = true;
		},
		hidePopup() {
			this.$emit('overlay-was-clicked');
		},
		share() {
			this.$emit('share-was-clicked', this.shareText);
			this.hidePopup();
			this.shareText = '';
		},
		blurTextarea() {
			this.textareaWasFocused = false;
			this.$refs.textarea.$el.blur();
		},
		togglePrivacyPopup() {
			this.privacyPopupIsVisible = !this.privacyPopupIsVisible;
		},
		selectPrivacySetting(setting) {
			this.privacySetting = setting;
		}
	},
	template: `
		<div v-click-outside="hidePopup" class="share-popup-wrapper">
			<transition :name="popupsAnimationName">
				
				<privacy-popup 
					v-if="privacyPopupIsVisible"
					key="privacyPopup"
					v-model="privacySetting"
					@hide-was-clicked="togglePrivacyPopup"></privacy-popup>
					
				<div 
					v-else 
					key="sharePopup"
					class="share-popup-inner">
					<transition-group name="slide-bottom">
						<div key="popup" class="share-popup with-grip">
							<div class="share-popup__full-screen">
								<img src="./img/maximize.svg" alt="">
							</div>
							<div class="new-post">
								<div class="new-post__head-wrapper">
									<transition-group name="slide-back-button" tag="div">
										<button 
											v-if="textareaWasFocused"
											key="back" 
											class="new-post__back"
											@click="blurTextarea">
											<img src="./img/back-arrow.svg" alt="">
										</button>
										<div key="head" class="new-post__head">
											<figure class="new-post__avatar">
												<img :src="screenSettings.avatar" class="new-post__avatar-img" alt="">
											</figure>
											<div class="new-post__container">
												<div class="new-post__name">
													{{ screenSettings.name }}
												</div>
												<div class="new-post__tools">
													<div class="new-post-tool">
														{{ $root.currentLang.repostPopup.postTool }}
													</div>
													<div class="new-post-tool"
														@click="togglePrivacyPopup">
														{{ privacySetting.title }}
													</div>
												</div>
											</div>
										</div>
									</transition-group>
								</div>
				
								<outgoing-message
										class="new-post__message"
										:placeholder="$root.currentLang.repostPopup.placeholder"
										v-model="shareText"
										ref="textarea"
										@focus.native="showKeyboard"></outgoing-message>
								<div class="share-popup__button-holder">
									<button class="share-popup__button" @click="share">
										{{ $root.currentLang.repostPopup.shareButton }}
									</button>
								</div>
								<ul class="share-popup-list">
									<li class="share-popup-list__item">
										<img src="./img/send-messenger.svg" alt="" class="share-popup-list__icon">
										{{ $root.currentLang.repostPopup.shareLinks[0] }}
									</li>
									<li class="share-popup-list__item">
										<img src="./img/copy.svg" alt="" class="share-popup-list__icon">
										{{ $root.currentLang.repostPopup.shareLinks[1] }}
									</li>
								</ul>
							</div>
						</div>
						
						
						<ios-keyboard v-if="textareaWasFocused" key="keyboard" :lang="screenSettings.currentLang" ref="keyboard"></ios-keyboard>
					</transition-group>
				</div>
			</transition>
		</div>
	`,
}