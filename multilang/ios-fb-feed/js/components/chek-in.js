import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';

export default {
	name: 'check-in',
	components: {
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard
	},
	props: {
		screenSettings: Object
	},
	data() {
		return {
			newCheckInText: '',
			fieldWasFocused: false
		}
	},
	methods: {
		showKeyboard() {
			this.fieldWasFocused = true;
		},
		hideScreen() {
			this.$emit('hide-was-clicked')
		},
		sharePost() {
			this.$emit('share-was-clicked', this.newCheckInText, true, 'check-in');
			this.newCheckInText = '';
			this.fieldWasFocused = false;
			this.hideScreen()
		},
	},
	template: `
	<div class="new-publication-screen">
		<div class="new-publication-screen__header new-publication-screen-header">
			<div class="new-publication-screen-header__cancel" @click="hideScreen">
				{{ $root.currentLang.newCheckIn.header[0] }}
			</div>
			<div class="new-publication-screen-header__title">
				{{ $root.currentLang.newCheckIn.header[1] }}
			</div>
			<div class="new-publication-screen-header__share" @click="sharePost">
				{{ $root.currentLang.newCheckIn.header[2] }}
			</div>
		</div>
		<div class="new-publication-screen__main" :class="{ 'new-publication-screen__main--with-active-keyboard' : fieldWasFocused }">
			<div class="new-post">
				<div class="new-post__head">
					<figure class="new-post__avatar">
						<img :src="screenSettings.avatar" class="new-post__avatar-img" alt="">
					</figure>
					<div class="new-post__container">
						<div class="new-post__name">
							{{ screenSettings.name }}
						</div>
						<div class="new-post__tools">
							<div class="new-post-tool new-post-tool--globe">
								{{ $root.currentLang.newPost.tools[0] }}
							</div>
							<div class="new-post-tool new-post-tool--plus">
								{{ $root.currentLang.newPost.tools[1] }}
							</div>
						</div>
					</div>
				</div>

				<outgoing-message
						class="new-post__message"
						:placeholder="$root.currentLang.newCheckIn.placeholder"
						v-model="newCheckInText"
						ref="textarea"
						@focus.native="showKeyboard"></outgoing-message>
			</div>
			<figure class="new-post-attachment">
				<div class="new-post-attachment__edit">
					<img src="./img/edit.svg" class="new-post-attachment__edit-img" alt="">
					Edit
				</div>
				<button class="new-post-attachment__remove">
					<img src="./img/cross.svg" alt="">
				</button>
				<img :src="screenSettings.newCheckInAttachment" class="new-post-attachment__img flexible-img" alt="">
			</figure>
		</div>
		<div class="keyboard-wrapper">
			<transition-group name="keyboard-slide">
				<div key="caption" class="keyboard-additional" :class="{'keyboard-additional--without-grip' : fieldWasFocused}">
					<div class="keyboard-additional__caption">
						{{ $root.currentLang.newPost.additional }}
					</div>
					<ul class="keyboard-additional__icons">
						<li class="keyboard-additional__icon">
							<img src="./img/photos.svg" alt="">
						</li>
						<li class="keyboard-additional__icon">
							<img src="./img/user-tag.svg" alt="">
						</li>
						<li class="keyboard-additional__icon">
							<img src="./img/smile-yellow.svg" alt="">
						</li>
						<li class="keyboard-additional__icon">
							<img src="./img/map.svg" alt="">
						</li>
					</ul>
				</div>
				<ios-keyboard v-if="fieldWasFocused" key="keyboard" :lang="screenSettings.currentLang" ref="keyboard"></ios-keyboard>
			</transition-group>
		</div>
	</div>
	`
}