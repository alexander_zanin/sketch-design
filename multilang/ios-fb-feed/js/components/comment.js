export default {
	name: 'comment',
	props: {
		parentComment: Object,
		comment: Object,
		isSmall: Boolean
	},
	mounted() {
		this.$emit('new-comment-was-added', this.$refs.comment);
	},
	methods: {
		replyComment() {
			this.$emit('reply-was-clicked', {
				comment: this.comment,
				parentComment: this.parentComment,
				commentDomElement: this.$refs.comment
			});
		},
		likeComment() {
			this.$emit('like-was-clicked', this.comment);
		}
	},
	template: `
	<li class="post-comment">
		<figure class="post-comment__avatar rounded" :class="{ 'post-comment__avatar--small' : isSmall }">
			<img :src="comment.avatar" class="post-comment__img flexible-img" alt="">
		</figure>
		<div class="post-comment__holder">
			<div ref="comment" class="post-comment__container">
				<div class="post-comment__bubble">
					<strong class="post-comment__name">
						{{ comment.name }}
					</strong>
					<p class="post-comment__text">
						{{ comment.text }}
					</p>
					<div v-if="comment.likesCount" class="post-comment__likes">
						<img src="./img/comment-like.svg" alt="">
						{{ comment.likesCount }}
					</div>
				</div>
				<div class="post-comment__meta">
					<div class="post-comment__meta-container">
						<span class="post-comment__meta-time">
							{{ comment.time }}
						</span>
						<ul class="post-comment-meta-list">
							<li class="post-comment-meta-list__item"
								:class="{ 'post-comment-meta-list__item--blue' : comment.isLiked }"
								@click="likeComment">
								{{ $root.currentLang.comment.like }}
							</li>
							<li class="post-comment-meta-list__item" @click="replyComment">
								{{ $root.currentLang.comment.reply }}
							</li>
						</ul>
					</div>
				</div>
			</div>
			<slot></slot>
		</div>
	</li>
	`
}