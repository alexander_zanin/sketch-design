import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';

export default {
	name: 'new-post-screen',
	components: {
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard
	},
	props: {
		screenSettings: Object
	},
	data() {
		return {
			newPostText: '',
			fieldWasFocused: false,
			BIG_SYMBOLS_LENGTH: 82,
			HIDE_TOOLS_LENGTH: 190,
		}
	},
	methods: {
		showKeyboard() {
			this.fieldWasFocused = true;
		},
		sharePost() {
			this.$emit('share-was-clicked', this.newPostText, false);
			this.newPostText = '';
			this.fieldWasFocused = false;
		},
	},
	template: `
	<div class="new-post-screen new-publication-screen">
		<div class="new-post-screen__header new-publication-screen-header">
				<div class="new-publication-screen-header__cancel" @click="$emit('hide-was-clicked')">
					{{ $root.currentLang.newPost.header[0] }}
				</div>
				<div class="new-publication-screen-header__title">
					{{ $root.currentLang.newPost.header[1] }}
				</div>
				<figure class="new-publication-screen-header__share" @click="sharePost">
					{{ $root.currentLang.newPost.header[2] }}
				</figure>
			</div>
		<div class="new-post-screen__main">
			<div class="new-post">
				<div class="new-post__head">
					<figure class="new-post__avatar">
						<img :src="screenSettings.avatar" class="new-post__avatar-img" alt="">
					</figure>
					<div class="new-post__container">
						<div class="new-post__name">
							{{ screenSettings.name }}
						</div>
						<div class="new-post__tools">
							<div class="new-post-tool new-post-tool--globe">
								{{ $root.currentLang.newPost.tools[0] }}
							</div>
							<div class="new-post-tool new-post-tool--plus">
								{{ $root.currentLang.newPost.tools[1] }}
							</div>
						</div>
					</div>
				</div>
	
				<outgoing-message
						:class="{ 'message-text--fz-small' : newPostText.length > BIG_SYMBOLS_LENGTH }"
						:placeholder="$root.currentLang.newPost.placeholder"
						v-model="newPostText"
						ref="textarea"
						@focus.native="showKeyboard"></outgoing-message>
			</div>
	
			<transition name="slide-fade" mode="out-in">
				<div v-if="fieldWasFocused" class="keyboard-wrapper">
					<div v-if="newPostText.length < HIDE_TOOLS_LENGTH" class="keyboard-tools">
						<img src="./img/tools.jpg" class="keyboard-tools__img flexible-img" alt="">
					</div>
					<div class="keyboard-additional">
						<div class="keyboard-additional__caption">
							{{ $root.currentLang.newPost.additional }}
						</div>
						<ul class="keyboard-additional__icons">
							<li class="keyboard-additional__icon">
								<img src="./img/photos.svg" alt="">
							</li>
							<li class="keyboard-additional__icon">
								<img src="./img/user-tag.svg" alt="">
							</li>
							<li class="keyboard-additional__icon">
								<img src="./img/smile.svg" alt="">
							</li>
							<li class="keyboard-additional__icon">
								<img src="./img/map.svg" alt="">
							</li>
						</ul>
					</div>
					<ios-keyboard :lang="screenSettings.currentLang" ref="keyboard"></ios-keyboard>
				</div>
	
				<ul v-else class="attachments">
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/photos.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[0] }}
					</li>
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/smile.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[1] }}
					</li>
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/map.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[2] }}
					</li>
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/video.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[3] }}
					</li>
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/camera-blue.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[4] }}
					</li>
					<li class="attachments__item">
						<figure class="attachments__icon">
							<img src="./img/tag.svg" alt="">
						</figure>
						{{ $root.currentLang.newPost.attachments[5] }}
					</li>
				</ul>
			</transition>
		</div>
	</div>
	`
}