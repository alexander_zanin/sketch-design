export default {
	name: 'post',
	props: {
		post: Object
	},
	template: `
	<ul class="post__tools bordered-post">
		<li class="post__tool" @click="$emit('like-was-clicked', post)" :class="{ 'post__tool--blue' : post.isLiked }">
			<transition name="like" mode="out-in">
				<img 
					v-if="post.isLiked" 
					key="liked" 
					src="./img/like-active.svg" 
					class="post__tool-img icon-like-active" alt="">
				<img 
					v-else 
					key="notLiked" 
					src="./img/like.svg" 
					class="post__tool-img icon-like" alt="">
			</transition>
			{{ $root.currentLang.post.tools[0] }}
		</li>
		<li class="post__tool" @click="$emit('comments-was-clicked', post.id)">
			<img src="./img/comment.svg" class="post__tool-img" alt="">
			{{ $root.currentLang.post.tools[1] }}
		</li>
		<li class="post__tool" @click="$emit('share-was-clicked', post)">
			<img src="./img/share.svg" class="post__tool-img" alt="">
			{{ $root.currentLang.post.tools[2] }}
		</li>
	</ul>
	`
}