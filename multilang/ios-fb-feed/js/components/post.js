import PostMeta from './post-meta'
import PostAttachments from './post-attachments'
import PostInfo from './post-info'
import PostTools from './post-tools'

export default {
	name: 'post',
	components: {
		'post-meta': PostMeta,
		'post-attachments': PostAttachments,
		'post-info': PostInfo,
		'post-tools': PostTools
	},
	props: {
		post: Object,
		isSelected: {
			type: Boolean,
			default: false
		}
	},
	template: `
	<div class="post card">
		<div class="post__info">
			<figure v-if="isSelected" class="post__back" @click="$emit('back-was-clicked')">
				<img src="./img/chevron-down.svg" alt="" style="transform: rotate(90deg)">
			</figure>
			<figure class="post__avatar rounded" v-if="post.avatar">
				<img :src="post.sharedPerson ? post.sharedPerson.avatar : post.avatar" alt="" class="post__avatar-img flexible-img">
			</figure>
			<post-info :post="post" :is-shared-person="!!post.sharedPerson">
				<span slot="caption">{{ post.caption }}</span>
			</post-info>
			<img src="./img/post-more.svg" class="post__more" alt="">
		</div>
		
		<div v-if="post.sharedPerson" class="post__text">
			{{ post.sharedPerson.text }}
		</div>
		
		<div v-if="post.isShared" class="post__shared post-shared">
			<div class="post__info">
				<figure class="post__avatar rounded">
					<img :src="post.avatar" alt="" class="post__avatar-img flexible-img">
				</figure>
				<post-info :post="post" :with-tagged-caption="false"></post-info>
			</div>
			<div v-if="post.text" class="post__text">
				{{ post.text }}
			</div>
		</div>
		
		<div v-else class="post__text">
			{{ post.text }}
		</div>
		
		<post-attachments v-if="post.attachment.exists" :post="post"></post-attachments>
		
		<post-meta v-if="!post.isShared" :post="post" :is-selected="isSelected"></post-meta>
		
		<!--<div v-if="post.isShared" class="post__shared post-shared">
			<div class="post__info">
				<figure class="post__avatar rounded">
					<img :src="post.sharedPerson.avatar" alt="" class="post__avatar-img flexible-img">
				</figure>
				<post-info :post="post.sharedPerson"></post-info>
			</div>
			<div class="post__text">
				{{ post.sharedPerson.text }}
			</div>
			<post-meta :post="post.sharedPerson" :is-selected="isSelected"></post-meta>
			<post-tools
				:post="post"
				@like-was-clicked="$emit('like-was-clicked', $event)"
				@comments-was-clicked="$emit('comments-was-clicked', $event)"
				@share-was-clicked="$emit('share-was-clicked', $event)"></post-tools>
		</div>-->
		
		<!--<div v-if="post.isShared" class="post__shared post-shared">
			<div class="post__info">
				<figure class="post__avatar rounded">
					<img :src="post.avatar" alt="" class="post__avatar-img flexible-img">
				</figure>
				<post-info :post="post" :with-tagged-caption="false"></post-info>
			</div>
			<div class="post__text">
				{{ post.text }}
			</div>
			<post-meta :post="post" :is-selected="isSelected"></post-meta>
			<post-tools
				:post="post"
				@like-was-clicked="$emit('like-was-clicked', $event)"
				@comments-was-clicked="$emit('comments-was-clicked', $event)"
				@share-was-clicked="$emit('share-was-clicked', $event)"></post-tools>
		</div>-->
		
		<div 
			v-if="isSelected && post.attachment.exists && post.attachment.pickedMode === 'withVideo'"
			class="post__views bordered-post">
			{{ post.attachment.views }}
		</div>
		
		<post-tools
			:post="post"
			@like-was-clicked="$emit('like-was-clicked', $event)"
			@comments-was-clicked="$emit('comments-was-clicked', $event)"
			@share-was-clicked="$emit('share-was-clicked', $event)"></post-tools>
	</div>
	`
}