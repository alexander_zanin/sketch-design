import Comment from './comment';

export default {
	name: 'comments-list',
	components: {
		'comment': Comment
	},
	props: {
		comments: Array,
	},
	methods: {
		emitEvent(e, eventName) {
			this.$emit(eventName, e);
		}
	},
	template: `
	<ul class="post-comments">
		<comment
				v-for="(comment, commentIndex) in comments"
				:key="commentIndex"
				:comment="comment"
				@new-comment-was-added="emitEvent($event, 'new-comment-was-added')"
				@like-was-clicked="emitEvent($event, 'like-was-clicked')"
				@reply-was-clicked="emitEvent($event, 'reply-was-clicked')">
	
			<div class="post-comment__replies" v-if="comment.comments && comment.comments.length">
				<comment
						v-for="(subComment, subCommentIndex) in comment.comments"
						:key="subCommentIndex"
						:is-small="true"
						:parent-comment="comment"
						:comment="subComment"
						@new-comment-was-added="emitEvent($event, 'new-comment-was-added')"
						@like-was-clicked="emitEvent($event, 'like-was-clicked')"
						@reply-was-clicked="emitEvent($event, 'reply-was-clicked')"></comment>
			</div>
		</comment>
	</ul>
	`
}