export default {
	name: 'post-info',
	props: {
		post: Object,
		withTaggedCaption: {
			type: Boolean,
			default: true
		},
		isSharedPerson: {
			type: Boolean,
			default: false
		},
		withTimeAndPlace: {
			type: Boolean,
			default: true
		}
	},
	template: `
	<div class="post__info-container">
		<div class="post__author">
			<strong class="post__author-name">
				{{ post.sharedPerson ? post.sharedPerson.name : post.name }}
			</strong>
			<template 
				v-if="post.taggedUsers && withTaggedCaption" 
				v-for="(taggedUser, tagIndex) in post.taggedUsers">
				<!--{{ tagIndex === 0 ? 'c ' : 'и' }}-->
				{{ $root.currentLang.post.and }}
				<strong class="post__author-name">
					{{ taggedUser }}
				</strong>
			</template>
			<slot name="caption"></slot>
			<strong v-if="post.checkedInPlace" class="post__checked-place">
				{{ post.checkedInPlace }}
			</strong>
			<span class="post__geotag post-geotag" v-if="post.location">
				{{ $root.currentLang.post.geotag }}
				<strong class="post-geotag__place">
					<img src="./img/geotag-gray.svg" class="post-geotag__img" alt="">
					{{ post.location }}
				</strong>
			</span>
		</div>
		<ul class="post__date-place">
			<li class="post__date">
				{{ isSharedPerson ? post.sharedPerson.time :post.time }}
			</li>
			<li class="post__date">
				{{ isSharedPerson ? post.sharedPerson.place :post.place }}
				<img src="./img/globe.svg" alt="">
			</li>
		</ul>
	</div>
	`
}