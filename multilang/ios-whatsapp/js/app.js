import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { showKeyboard } from '@common/mixins/show-keyboard';
import { currentLang } from '@common/mixins/current-lang';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import eventBus from '@common/tools/vue-event-bus';
import { tr } from './translations';


new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, showKeyboard, getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-whatsapp',
		interlocutor: {
			name: null,
			isTyping: false,
			isOnline: false
		},
		createGroupChat: true,
		participantsAmount: null,

		screenSettings: {
			avatar: './img/1a iosavatar.png',
			currentLang: 'rus',
			name: 'Нападение на американское…',
			participants: 'ГосДеп, Пентагон…',
			placeholder: '',
			defaultMessagesTime: '12:00',
			messages: [],
			additionalSettings: {
				messagesWithTime: false
			}
		}
	},
	watch: {
		screenSettings() {
			if (this.screenSettings.messages) return;
			this.screenSettings.messages = []
		}
	},
	created() {

		eventBus.$on('incoming-message-was-sent', this.addCustomTime);

		eventBus.$on('outgoing-message-was-sent', this.addCustomTime);
	},
	methods: {
		checkTypingStatus(interlocutorIsTyping, interlocutorName) {
			this.interlocutor.isTyping = interlocutorIsTyping;
			this.interlocutor.name = interlocutorName;
		},
		checkOnlineStatus(interlocutorIsOnline) {
			this.interlocutor.isOnline = interlocutorIsOnline;
		},
		addCustomTime(message) {
			message.customTime = this.screenSettings.defaultMessagesTime
		},
		clearChat() {
			this.screenSettings.messages = [];
		}
	}
});