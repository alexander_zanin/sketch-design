import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { currentLang } from '@common/mixins/current-lang';
import { showKeyboard } from '@common/mixins/show-keyboard';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import AndroidKeyboard from '@common/android-keyboard/vue-android-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { tr } from './translations';


new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'android-keyboard': AndroidKeyboard,
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, showKeyboard, getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'android-vk',
		interlocutor: {
			name: null,
			isTyping: false,
			isOnline: false
		},
		isFocused: false,
		createGroupChat: true,
		participantsAmount: 6,
		stickers: [
			'./img/stickers/sticker01.png'
		],

		screenSettings: {
			currentLang: 'rus',
			participants: '6 участников',
			avatar: './img/chatava.png',
			name: 'Название чата',
			messageDate: 'Сегодня, 2 Марта'
		}
	},
	methods: {
		checkTypingStatus(interlocutorIsTyping, interlocutorName) {
			this.interlocutor.isTyping = interlocutorIsTyping;
			this.interlocutor.name = interlocutorName;
		},
		checkOnlineStatus(interlocutorIsOnline) {
			this.interlocutor.isOnline = interlocutorIsOnline;
		},
		removeUnread() {
			this.isFocused = true;
		}
	}
});