export const tr = {
	rus: {
		placeholder: 'Ваше сообщение',
		typingMessage: 'печатает'
	},

	en: {
		placeholder: 'Your message',
		typingMessage: 'is typing'
	},

	fr: {
		placeholder: 'Entrez votre message',
		typingMessage: 'est en train d\'écrire'
	}
}