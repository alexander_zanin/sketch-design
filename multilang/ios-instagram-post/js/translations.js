export const tr = {
	rus: {
		head: 'Публикация',
		comments: {
			head: 'Комментарии',
			placeholder: 'Комментировать',
			btn: 'Опубликовать',
			replyBtn: 'Ответить',
			replyMsg: 'В ответ'
		}
	},

	en: {
		head: 'Publication',
		comments: {
			head: 'Comments',
			placeholder: 'Add a comment...',
			btn: 'Post',
			replyBtn: 'Reply',
			replyMsg: 'Replying to'
		}
	},

	fr: {
		head: 'Publication',
		comments: {
			head: 'Commentaires',
			placeholder: 'Ajouter un commentaire...',
			btn: 'Publier',
			replyBtn: 'Répondre',
			replyMsg: 'Réponse a'
		}
	}
}