const filters = {
	latin(number) {
		if (number > 1) return `${number} likes`;
		return `${number} like`;
	},
	rus(number) {
		if (number / 10 >= 1 && number / 10 < 2) {
			return `${number} отметок «Нравится»`;
		}
		if (number % 10 === 1) {
			return `${number} отметка «Нравится»`;
		}
		const lastN = number.toString().split('').pop();

		const range = '234';

		if (range.includes(lastN)) {
			return `${number} отметки «Нравится»`;
		} else  {
			return `${number} отметок «Нравится»`;
		}
	}
};

export default {
	name: 'comment',
	props: {
		parentComment: Object,
		comment: Object,
		isSmall: Boolean,
		lang: String
	},
	mounted() {
		this.$emit('new-comment-was-added', this.$refs.comment);
	},
	filters: {
		likesEnding(number, lang) {
			if (lang === 'rus') {
				return filters.rus(number);
			}

			return filters.latin(number);
		}
	},
	methods: {
		replyComment() {
			this.$emit('reply-was-clicked', {
				comment: this.comment,
				parentComment: this.parentComment,
				commentDomElement: this.$refs.comment
			});
		},
		likeComment() {
			this.$emit('like-was-clicked', this.comment);
		}
	},
	template: `
	<div class="comment" key="comment">
		<figure class="comment__avatar rounded">
			<img :src="comment.avatar" class="comment__avatar-img flexible-img" alt="">
		</figure>
		<div class="comment__container">
			<div class="comment__inner" ref="comment">
				<div class="comment__main">
					<div class="comment__text">
						<strong class="comment__name">
							{{ comment.name }}
						</strong>
						{{ comment.text }}
					</div>
					<div class="comment__bar">
						<span class="comment__time">{{ comment.time }}</span>
						<ul class="comment-meta-list">
							<li v-if="comment.likesCount" class="comment-meta-list__item">{{ comment.likesCount | likesEnding(lang) }}</li>
							<li
								class="comment-meta-list__item"
								@click="replyComment">{{ $root.currentLang.comments.replyBtn }}</li>
						</ul>
					</div>
				</div>
				<figure class="comment__like" @click="likeComment">
					<img v-if="comment.isLiked" src="./img/like-small-filled.svg" class="flexible-img" alt="">
					<img v-else src="./img/like-small.svg" class="flexible-img" alt="">
				</figure>
			</div>
			<slot></slot>
		</div>
	</div>
	`
}