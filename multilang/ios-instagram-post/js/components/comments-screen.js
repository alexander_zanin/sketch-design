import { generateId } from '../../../commonComponents/helpers/generate-id';
import { showKeyboard } from '../../../commonComponents/mixins/show-keyboard';
import Comment from './comment';
import OutgoingMessage from '../../../commonComponents/messaging/vue-outgoing-message';
import IosKeyboard from '../../../commonComponents/ios-keyboard/vue-ios-keyboard';

export default {
	name: 'comments-screen',
	components: {
		'comment': Comment,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
	},
	mixins: [showKeyboard],
	props: {
		value: Object,
		avatar: String,
		username: String,
		lang: String
	},
	data() {
		return {
			post: {},
			outgoingMessage: '',
			commentToReply: null,
			parentComment: null
		}
	},
	created() {
		this.post = JSON.parse(JSON.stringify(this.value));
	},
	mounted() {
		setTimeout(() => {
			this.$refs.commentTextarea.$el.focus();
		}, 300);
	},
	methods: {
		sendComment() {
			const commentObj = {
				id: generateId(),
				name: this.username,
				avatar: this.avatar,
				text: this.outgoingMessage,
				time: '1с',
				likesCount: '',
				isLiked: false,
				comments: []
			};

			if (this.parentComment) {
				this.parentComment.comments.push(commentObj);
			} else if (this.commentToReply) {
				this.pushNewComment(this.commentToReply, commentObj);
			} else {
				this.pushNewComment(this.post, commentObj);
			}

			this.cancelReplying();
			this.outgoingMessage = '';

			const scrollable = this.$refs.commentScreenContainer;

			this.$nextTick(() => {
				scrollable.scrollTop = scrollable.scrollHeight;
				this.$refs.commentTextarea.$el.focus();
			});


			this.$emit('input', this.post);
		},
		pushNewComment(parentObj, newComment) {
			if (!parentObj.comments) {
				this.$set(parentObj, 'comments', []);
			}
			parentObj.comments.push(newComment);
		},
		cancelReplying() {
			this.parentComment = null;
			this.commentToReply = null;
		},
		toggleLike(obj) {
			obj.isLiked = !obj.isLiked;

			const likesCount = +obj.likesCount;

			if (obj.isLiked) {
				obj.likesCount = likesCount + 1;
			} else {
				obj.likesCount = likesCount - 1;
			}

			this.$emit('input', this.post);
		},
		showReplyMessage({ comment, parentComment, commentDomElement }) {
			this.commentToReply = comment;
			this.parentComment = parentComment;
			this.$refs.commentTextarea.$el.focus();
			this.outgoingMessage = `@${comment.name} `;

			this.scrollToComment(commentDomElement);
		},
		scrollToComment(commentDomElement) {
			this.$nextTick(() => {
				const holder = this.$refs.commentScreenContainer;
				const holderHWithoutPB = holder.offsetHeight - parseInt(getComputedStyle(holder, null).paddingBottom);
				holder.scrollTop = (commentDomElement.offsetTop - holderHWithoutPB) + commentDomElement.offsetHeight;
			});
		}
	},
	template: `
	<div class="comments-screen">
		<div class="comments" ref="commentScreenContainer">
			<!--<transition-group name="slide-fade" tag="div">-->
				<comment
						v-for="(comment, commentIndex) in post.comments"
						:key="commentIndex"
						:comment="comment"
						:lang="lang"
						@new-comment-was-added="scrollToComment"
						@like-was-clicked="toggleLike"
						@reply-was-clicked="showReplyMessage">
			
					<div class="comment__replies" v-if="comment.comments && comment.comments.length">
						<comment
								v-for="(subComment, subCommentIndex) in comment.comments"
								class="comment--small"
								:key="subCommentIndex"
								:is-small="true"
								:parent-comment="comment"
								:lang="lang"
								:comment="subComment"
								@new-comment-was-added="scrollToComment"
								@like-was-clicked="toggleLike"
								@reply-was-clicked="showReplyMessage"></comment>
					</div>
				</comment>
			<!--</transition-group>-->
		</div>
	
		<div class="comments-input" ref="inputBar">
			<div v-if="commentToReply" class="comments-input__reply">
				{{ $root.currentLang.comments.replyMsg }} {{ commentToReply.name }}
				<button class="comments-input__reply-close" @click="cancelReplying">
					✕
				</button>
			</div>
			<div class="comments-input__main">
				<figure class="comments-input__avatar rounded">
					<img :src="avatar" class="flexible-img" alt="">
				</figure>
				<div class="comments-input__holder">
					<outgoing-message
							:placeholder="$root.currentLang.comments.placeholder"
							v-model="outgoingMessage"
							ref="commentTextarea"
							@focus.native="showKeyboard"></outgoing-message>
					<button
							@click="sendComment"
							:disabled="!this.outgoingMessage"
							class="comments-input__send">
						{{ $root.currentLang.comments.btn }}
					</button>
				</div>
	
				<ios-keyboard :lang="lang" ref="keyboard"></ios-keyboard>
			</div>
		</div>
	</div>
	`
}