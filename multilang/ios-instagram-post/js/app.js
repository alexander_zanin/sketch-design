import Vue from '@common/vue';
import { generateId } from '@common/helpers/generate-id';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import CommentsScreen from './components/comments-screen';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';
import SwitchLang from '@common/switch-lang/switch-lang';
import { currentLang } from '@common/mixins/current-lang';
import { tr } from './translations';

new Vue({
	el: '#app',
	components: {
		'comments-screen': CommentsScreen,
		'settings': screenSettings,
		'loading-overlay': LoadingOverlay,
		'switch-lang': SwitchLang
	},
	mixins: [getscreenSettings, currentLang(tr)],
	data: {
		databaseRef: 'ios-instagram-post',
		showComments: false,
		screenSettings: {
			currentLang: 'rus',
			username: 'anothername',
			avatar: './img/profile-avatar.png',
			photo: './img/img01.jpg',
			likes: 'Нравится этим людям и еще 150',
			date: '25 февраля',
			inputPlaceholder: 'Add a comment...',
			isLiked: false,
			comments: [
				{
					id: generateId(),
					name: 'username',
					avatar: './img/post-avatar.png',
					text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
					time: '1w',
					likesCount: 1,
					isLiked: false,
					comments: []
				},
				{
					id: generateId(),
					name: 'username2',
					avatar: './img/post-avatar.png',
					text: 'Lorem ipsum.',
					time: '1h',
					likesCount: 0,
					isLiked: false,
					comments: []
				},
				{
					id: generateId(),
					name: 'username3',
					avatar: './img/post-avatar.png',
					text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
					time: '1h',
					likesCount: 0,
					isLiked: false,
					comments: []
				}
			]
		}
	},
	methods: {
		toggleLike(post) {
			post.isLiked = !post.isLiked
		},
		animationName() {
			if (this.showComments) {
				return 'slide';
			}
			return 'slide-reverse';
		},
		goToComments() {
			this.showComments = true;
		},
	}
});