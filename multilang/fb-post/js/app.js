import Vue from '@common/vue';
import { sendMessage } from '@common/mixins/send-message';
import { showKeyboard } from '@common/mixins/show-keyboard';
import IosKeyboard from '@common/ios-keyboard/vue-ios-keyboard';
import IncomingMessage from '@common/messaging/vue-incoming-message';
import MessagesList from '@common/messaging/vue-messages-list';
import OutgoingMessage from '@common/messaging/vue-outgoing-message';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import SwitchLang from '@common/switch-lang/switch-lang';

new Vue({
	el: '#app',
	components: {
		'incoming-message': IncomingMessage,
		'messages-list' : MessagesList,
		'outgoing-message': OutgoingMessage,
		'ios-keyboard': IosKeyboard,
		'chat-settings': screenSettings,
		'switch-lang': SwitchLang
	},
	mixins: [sendMessage, showKeyboard],
	data: {
		textareaWasClicked: false,
		databaseRef: 'fb-post',
		screenSettings: {
			currentLang: 'rus'
		}
	},
	methods: {
		checkFocus() {
			this.textareaWasClicked = true;
		}
	}
});