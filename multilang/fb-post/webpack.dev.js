const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = merge(common, {
	mode: 'development',
	watch: true,
	// devServer: {
	// 	contentBase: path.join(__dirname, ''),
	// 	compress: true,
	// 	port: 8080
	// },
	plugins: [
		new BrowserSyncPlugin({
			host: 'localhost',
			port: 3000,
			proxy: 'http://localhost:8080'
		})
	]
})