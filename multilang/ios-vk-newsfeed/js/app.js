import Vue from '@common/vue';
import { getscreenSettings } from '@common/mixins/get-chat-settings';
import screenSettings from '@common/chat-settings/vue-chat-settings';
import LoadingOverlay from '@common/chat-settings/vue-loading-overlay';

new Vue({
	el: '#app',
	components: {
		'chat-settings': screenSettings,
		'loading-overlay': LoadingOverlay
	},
	mixins: [getscreenSettings],
	data: {
		databaseRef: 'ios-vk-newsfeed',
		screenSettings: {
			messagesNotificationsCount: 12,
			stories: [
				{
					name: 'Михаил',
					avatar: './img/story01.png',
				},
				{
					name: 'Михаил',
					avatar: './img/story02.png',
					isUpdated: true
				},
				{
					name: 'Юрий',
					avatar: './img/story03.png',
					isUpdated: true
				},
				{
					name: 'Семён',
					avatar: './img/story04.png',
				},
				{
					name: 'Тимофей',
					avatar: './img/story05.png',
				}
			],
			posts: [
				{
					avatar: './img/avatar01.png',
					name: 'Александр Щепилов',
					time: 'сегодня в 9:41',
					text: 'Хочу iPhone X только ради того, чтобы играться с анимоджи, но пока что рассудок побеждает. Надолго ли?',
					attachment: {
						img: './img/img01.png',
						name: 'Don\'t buy iPhone X',
						views: '166 260 просмотров'
					},
					likesCount: 26,
					commentsCount: 4,
					repliesCount: 1,
					views: 1200,
					isLiked: true,
					friendsWhoLikesAvatars: ['./img/likes01.png'],
					friendsWhoLikesCaption: 'Понравилось Муртолу Лазвачеву'
				}
			]
		}
	},
	methods: {
		toggleLike(post) {
			post.isLiked = !post.isLiked;
			if (post.isLiked) {
				post.likesCount += 1;
				return;
			}
			post.likesCount -= 1;
		},
		getCount(count) {
			if (count >= 1000) {
				const num = parseFloat(count/1000);
				const rounded = Math.round(num * 10) / 10;

				return `${rounded}K`;
			}
			return count;

		},
		addNewPost() {
			this.screenSettings.posts.unshift({
				avatar: './img/avatar-default.png',
				name: 'User Name',
				time: 'сегодня в 9:41',
				text: 'Some text',
				attachment: {
					img: './img/img01.png',
					name: 'Some title',
					views: '166 260 просмотров'
				},
				likesCount: 4129,
				commentsCount: 2145,
				repliesCount: 1672,
				views: '9.6K',
				isLiked: false,
				friendsWhoLikesAvatars: ['./img/avatar-default.png'],
				friendsWhoLikesCaption: 'Понравилось Кому-то'
			});
		}
	}
});